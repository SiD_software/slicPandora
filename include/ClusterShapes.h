#ifndef ClusterShapes_h
#define ClusterShapes_h

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cstdlib>

#include <math.h>

/**
 *    Ported from MarlinUtil.  Only took findGravity() and getCentreOfGravity() functions.
 *    There is no GSL dependency.  
 *    --JM
 *
 *    @authors V. Morgunov (ITEP/DESY), A. Raspereza (DESY), O. Wendt (DESY)
 *    @version $Id: ClusterShapes.h,v 1.2 2010/06/08 22:12:41 jeremy Exp $
 *
 */
class ClusterShapes 
{

public:

    /**
     *    Constructor
     *    @param nhits : number of hits in the cluster
     *    @param a     : amplitudes of elements ('cells') of the cluster. Stored in 
     *                   an array, with one entry for each element ('cell'). Each entry 
     *                   is depending on coordinates x,y,z (Cartesian), which are stored 
     *                   in the arrays x,y,z.
     *    @param x,y,z : array of coordinates corresponding to the array of amplitudes a.
     *
     *
     */
    ClusterShapes(int nhits, float* a, float* x, float* y, float* z);


    /**
     *    Destructor
     */
    ~ClusterShapes();

    /**
     * returns an array, which represents a vector from the origin of the
     * coordiante system, i.\ e.\ IP, to the centre of gravity of the cluster. The centre 
     * of gravity is calculated with the energy of the entries of the cluster.
     */
    float* getCentreOfGravity();

    /** US spelling of getCentreOfGravity */
    inline float* getCenterOfGravity() { return getCentreOfGravity() ; }

private:

    void  findGravity();

private:

    int _nHits;

    float* _aHit;
    float* _xHit;
    float* _yHit;
    float* _zHit;
    float* _exHit;
    float* _eyHit;
    float* _ezHit;
    int* _types;
    float* _xl;
    float* _xt;
    float* _t;
    float* _s;

    int   _ifNotGravity;
    float _totAmpl;
    float _radius;
    float _xgr;
    float _ygr;
    float _zgr;
    float _analogGravity[3];

    int   _ifNotWidth;
    float _analogWidth;

    int   _ifNotInertia;
    float _ValAnalogInertia[3];
    float _VecAnalogInertia[9];

    int _ifNotEigensystem;

    int   _ifNotElipsoid;
    float _r1           ;  // Cluster spatial axis length -- the largest
    float _r2           ;  // Cluster spatial axis length -- less
    float _r3           ;  // Cluster spatial axis length -- less
    float _vol          ;  // Cluster ellipsoid volume
    float _r_ave        ;  // Cluster average radius  (qubic root)
    float _density      ;  // Cluster density
    float _eccentricity ;  // Cluster Eccentricity
    float _r1_forw      ;
    float _r1_back      ; 

};

#endif
