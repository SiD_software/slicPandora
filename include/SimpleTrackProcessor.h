// $Id: SimpleTrackProcessor.h,v 1.4 2010/10/06 22:51:22 jeremy Exp $
#ifndef SIMPLETRACKPROCESSOR_H
#define SIMPLETRACKPROCESSOR_H 1

// slicPandora
#include "EventProcessor.h"

// lcio
#include "EVENT/LCEvent.h"

// pandora
#include "Api/PandoraApi.h"

using EVENT::LCEvent;

/**
 * This class converts from LCIO Tracks to PandoraApi::Track::Parameters
 * by reading in the "Tracks" collection from LCSim output events, as well
 * as three LCGenericObject collections containing TrackStates (x,y,z,px,py,pz)
 * at the track start, at the ECal face, and at the track end point.  It will
 * also setup links between Tracks and their associated MCParticles if the 
 * LCRelations collection is present.
 */
class SimpleTrackProcessor : public EventProcessor
{

public:

    SimpleTrackProcessor()
        : EventProcessor("SimpleTrackProcessor")
    {;}

    virtual ~SimpleTrackProcessor()
    {;}

public:
    
    void processEvent(EVENT::LCEvent*);

private:

    //void setupTrackStatesFromRelations(PandoraApi::Track::Parameters&, EVENT::LCEvent*);
    void setupTrackStatesFromGenericObjects(PandoraApi::Track::Parameters&, EVENT::LCEvent*, int);

private:

    static std::string trackCollectionName;
    //static std::string startCollectionName;
    //static std::string ecalCollectionName;
    //static std::string endCollectionName;
};

#endif
