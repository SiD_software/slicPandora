#ifndef MCPARTICLEPROCESSOR_H
#define MCPARTICLEPROCESSOR_H 1

// pandora
#include "Api/PandoraApi.h"

// slicPandora
#include "EventProcessor.h"
#include "DetectorGeometry.h"

// lcio
#include "EVENT/MCParticle.h"

using EVENT::MCParticle;

/**
 * This is an event processor that converts collections of MCParticles
 * to Pandora MCParticle::Parameters and registers them with the current
 * Pandora instance.  
 */
class MCParticleProcessor : public EventProcessor
{
public:

    /**
     * Standard ctor.
     */
    MCParticleProcessor()
        : EventProcessor("MCParticleProcessor")
    {;}

    /**
     * Standard dtor.
     */
    virtual ~MCParticleProcessor()
    {;}

public:

    /**
     * This method converts LCIO MCParticles into Pandora CalHit::Parameters.
     */
    void processEvent(EVENT::LCEvent*);

private:
    
/*     /\** */
/*      * Make a 64-bit ID from the two 32-bit cell IDs of a MCParticle. */
/*      *\/ */
/*     inline long long makeId64(MCParticle* hit) const */
/*     {         */
/*         return ((long long)hit->getCellID1())<<32 | hit->getCellID0(); */
/*     } */

    /**
     * Convert an LCIO MCParticle into a PandoraPFANew MCParticle Parameters.
     */
    PandoraApi::MCParticle::Parameters makeMCParticleParameters(MCParticle*, int);

    /**
     * Print the given MCParticle::Parameters to cout.
     */
    void printMCParticleParameters(const PandoraApi::MCParticle::Parameters&);

private:
    static std::string mcParticleCollectionName;

};

#endif
