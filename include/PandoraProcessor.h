#ifndef PANDORAPROCESSOR_H
#define PANDORAPROCESSOR_H 1

// slicPandora
#include "EventProcessor.h"

// lcio
#include "EVENT/LCEvent.h"

/**
 * Calls ProcessEvent to activate Pandora on an event.
 */
class PandoraProcessor : public EventProcessor
{
public:
    PandoraProcessor() 
        : EventProcessor("PandoraProcessor")
    {}
    
    virtual ~PandoraProcessor()
    {}

public:

    void processEvent(EVENT::LCEvent*);
};

#endif




