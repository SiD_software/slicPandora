// $Id: PandoraFrontend.h,v 1.1 2011/09/19 19:50:10 jeremy Exp $
#ifndef PANDORAFRONTEND_H_
#define PANDORAFRONTEND_H_

class PandoraFrontend
{
    public:
        PandoraFrontend() {;}
        virtual ~PandoraFrontend() {;}

        // To be run from program's main and used for return value.
        int run(int argc, char **argv);

        static void printUsage();
};

#endif /* PANDORAFRONTEND_H_ */
