// $Id: IDDecoder.h,v 1.4 2010/03/11 22:18:44 jeremy Exp $
#ifndef IDDECODER_H
#define IDDECODER_H 1

// stl
#include <string>
#include <vector>
#include <map>

/**
 * This class extracts field values from 64-bit identifiers based on an identifier 
 * description.  It provides similar functionality to the GeomConverter Java class
 * org.lcsim.geometry.util.IDDecoder (but NOT the org.lcsim.geometry.IDDecoder interface
 * with the same class name).
 */
class IDDecoder
{

public:

    /**
     * An IDField represents a portion of a 64-bit id which contains a data field.
     * This is defined by a starting bit, a length, and a flag that specifies
     * whether the field may contain signed values that need to be decoded differently
     * from unsigned fields.  Fields also have a name, e.g. "layer"..
     */
    class IDField
    {        
    public:

        /**
         * Fully specified ctor.  The parameters of IDFields do not change after creation.
         */
        IDField(const std::string& name, int start, int length, bool isSigned)
            : m_name(name), m_start(start), m_length(length), m_signed(isSigned)
        {;}

        /**
         * Standard dtor.
         */
        virtual ~IDField()
        {;}

        /**
         * Get the name of the field.
         */
        inline const std::string& getName() const
        {
            return m_name;
        }

        /**
         * Get the start bit of the field, numbered from 0 to 63.
         */
        inline const int getStart() const
        {
            return m_start;
        }

        /**
         * Get the length of the field.
         */
        inline const int getLength() const
        {
            return m_length;
        }

        /**
         * Get the sign flag specifying whether this field may contain signed field values.
         */
        inline const bool isSigned() const
        {
            return m_signed;
        }

    private:
        std::string m_name;
        int m_start;
        int m_length;
        bool m_signed;
    };

public:

    // List of fields.
    typedef std::vector<IDField*> IDFields;

    // Map of field indices to fields.
    typedef std::map<int,IDField*> IndexedFieldMap;

    // Map of field names to fields.
    typedef std::map<std::string,IDField*> NamedFieldMap;

    // Map field names to indices.
    typedef std::map<std::string,int> Name2IndexMap;

    /**
     * ctor that requires a list of fields.
     */
    IDDecoder(IDFields fields)
        : m_fields(fields)
    {
        // Setup the field maps.
        int i=0;
        for (IDFields::iterator iter = fields.begin();
             iter != fields.end();
             iter++)
        {
            m_indexMap[i] = (*iter);
            m_nameMap[(*iter)->getName()] = (*iter);
            m_name2IndexMap[(*iter)->getName()] = i;
            i++;
        }
    }
        
    /**
     * Standard dtor.
     */
    virtual ~IDDecoder()
    {;}
    
    /**
     * Extract a field at the given index from the id.
     */
    int getFieldValue(int index, long id);
    
    /**
     * Extract a field with the given name from the id.
     */
    int getFieldValue(const std::string& name, long id);
    
    /**
     * Get a field by index.
     */
    inline const IDField* getField(int index)
    {
        return m_indexMap[index];
    }

    /**
     * Get a field by name.
     */
    inline const IDField* getField(const std::string& name)
    {
        return m_nameMap[name];
    }

    /**
     * Get the index of a field by name.
     */
    inline const int getFieldIndex(const std::string& name)
    {
        return m_name2IndexMap[name];
    }
    
    /**
     * Get the number of fields.
     */
    inline const int getFieldCount() const
    {
        return m_fields.size();
    }

private:
    IDFields m_fields;
    IndexedFieldMap m_indexMap;
    NamedFieldMap m_nameMap;
    Name2IndexMap m_name2IndexMap;
};

#endif
