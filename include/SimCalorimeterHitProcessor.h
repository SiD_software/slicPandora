#ifndef SimCalorimeterHitProcessor_h
#define SimCalorimeterHitProcessor_h 1

#include "EventProcessor.h"

/**
 * This EventProcessor converts SimCalorimeterHits from slic output into CalorimeterHits suitable for Pandora input.
 * A sampling fraction is applied to the raw energy during this conversion process, and a relation table is
 * setup to connect the original SimCalorimeterHits to their converted CalorimeterHits.  The output CalorimeterHit
 * collection is given the same name as the CalorimeterType being processed (e.g. "EM_BARREL", "HAD_ENDCAP", etc.).
 * This class is based on the example code in slicPandora/src/CalorimeterHitMaker by Norman Graf.  In the future, 
 * this procedure may be removed from slicPandora and performed in a Java reconstruction processor in LCSim, instead.
 *
 * @author Jeremy McCormick
 */
class SimCalorimeterHitProcessor : public EventProcessor
{
public:
    SimCalorimeterHitProcessor();
    virtual ~SimCalorimeterHitProcessor();

public:
    void processEvent(EVENT::LCEvent*);

    // New impl.
    void processEventNew(EVENT::LCEvent*);
};

#endif
