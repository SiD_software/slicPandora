// $Id: EventMarkerProcessor.h,v 1.2 2010/06/08 22:12:41 jeremy Exp $

#ifndef EVENTMARKERPROCESSOR_H
#define EVENTMARKERPROCESSOR_H 1

#include "EventProcessor.h"

/**
 * Prints the event number for each event processed.
 */
class EventMarkerProcessor : public EventProcessor
{
public:
    EventMarkerProcessor() : EventProcessor("EventMarkerProcessor") {;}
    virtual ~EventMarkerProcessor() {;}

public:
    
    void processEvent(EVENT::LCEvent* event)
    {
        std::cout << std::endl;
        std::cout << ">>>>>> EVENT #" << event->getEventNumber() << std::endl;
        std::cout << std::endl;
    }

};

#endif
