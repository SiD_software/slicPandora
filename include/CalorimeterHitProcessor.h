#ifndef CALORIMETERHITPROCESSOR_H
#define CALORIMETERHITPROCESSOR_H 1

// pandora
#include "Api/PandoraApi.h"

// slicPandora
#include "EventProcessor.h"
#include "DetectorGeometry.h"

// lcio
#include "EVENT/CalorimeterHit.h"

using EVENT::CalorimeterHit;

namespace pandora { class SubDetector; }

/**
 * This is an event processor that converts collections of CalorimeterHits
 * to Pandora CaloHit::Parameters and registers them with the current
 * Pandora instance.  The collections to process are retrieved from the JobManager's
 * list of calorimeter collection types (e.g. EM_BARREL).
 */
class CalorimeterHitProcessor : public EventProcessor
{
public:

    /**
     * Standard ctor.
     */
    CalorimeterHitProcessor()
        : EventProcessor("CalorimeterHitProcessor")
    {;}

    /**
     * Standard dtor.
     */
    virtual ~CalorimeterHitProcessor()
    {;}

public:

    /**
     * This method converts LCIO CalorimeterHits into Pandora CalHit::Parameters.
     * @param The LCIO event containing the CalorimeterHit collections to convert.
     */
    void processEvent(EVENT::LCEvent* event);

private:
    
    /**
     * Make a 64-bit ID from the two 32-bit cell IDs of a CalorimeterHit.
     * @param The CalorimterHit with the ID to be converted.
     */
    inline long long makeId64(CalorimeterHit* hit) const
    {        
        return ((long long)hit->getCellID1())<<32 | hit->getCellID0();
    }

    /**
     * Convert an LCIO CalorimeterHit into a PandoraPFANew CaloHit Parameters.
     * @param subdet The SubDetector parameters for the hit.
     * @param xsubdet The extra SubDetector parameters.
     * @oaran calhit The CaloHit to be converted.
     * @return A Pandora CaloHit.
     */
    PandoraApi::CaloHit::Parameters makeCaloHitParameters(
        const pandora::SubDetector* subdet,
        DetectorGeometry::ExtraSubDetectorParameters* xsubdet,
        CalorimeterHit* calhit);

    /**
     * Print the given CalHit::Parameters to cout.
     * @param The CaloHit parameters to print out.
     */
    void printCaloHitParameters(const PandoraApi::CaloHit::Parameters& calparams);
};

#endif
