// $Id: ResetPandoraProcessor.h,v 1.3 2013/01/15 22:55:38 jeremy Exp $

#ifndef RESETPANDORAPROCESSOR_H
#define RESETPANDORAPROCESSOR_H 1

// slicPandora
#include "EventProcessor.h"

// pandora
#include "Api/PandoraApiImpl.h"

/**
 * This processor resets Pandora for the next event.
 */
class ResetPandoraProcessor : public EventProcessor
{
public:
    ResetPandoraProcessor() 
        : EventProcessor("ResetPandoraProcessor")
    {;}
    virtual ~ResetPandoraProcessor() {;}

public:
    void processEvent(EVENT::LCEvent*)
    {
        PandoraApi::Reset(getJobManager()->getPandora());
        // getJobManager()->getPandora().GetPandoraApiImpl()->ResetForNextEvent();
    }   
};


#endif
