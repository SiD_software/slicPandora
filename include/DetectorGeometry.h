// $Id: DetectorGeometry.h,v 1.16 2011/09/19 20:34:53 jeremy Exp $

#ifndef DetectorGeometry_h
#define DetectorGeometry_h 1

// pandora
#include "Api/PandoraApi.h"

// stl
#include <vector>

class IDDecoder;
class JobManager;

/**
 * Encapsulates the detector geometry including Pandora's geometry parameters,
 * as well as various extras necessary to load Pandora geometry files generated
 * by GeomConverter.
 */
class DetectorGeometry
{
    
public:

    // Extra layer parameters.
    class ExtraLayerParameters
    {
    public:
        pandora::InputFloat m_samplingFraction;
        pandora::InputFloat m_emSamplingFraction;
        pandora::InputFloat m_hadSamplingFraction;        
        pandora::InputFloat m_cellThickness;
        float m_intLength;
        float m_radLength;
    };

    typedef std::vector<ExtraLayerParameters> ExtraLayerParametersList;

    // Extra subdetector parameters, including cell sizes.
    class ExtraSubDetectorParameters
    {
    public:
        pandora::InputFloat m_cellThickness;
        pandora::InputFloat m_cellSizeU;
        pandora::InputFloat m_cellSizeV;
        pandora::InputFloat m_mipEnergy;
        pandora::InputFloat m_mipSigma;
        pandora::InputFloat m_mipCut;
        pandora::InputFloat m_timeCut;
        std::string m_collection;
        IDDecoder* m_decoder;
        ExtraLayerParametersList m_extraLayerParams;
        pandora::InputHitType m_inputHitType;
        pandora::InputHitRegion m_inputHitRegion;
        std::vector<pandora::CartesianVector*> m_normalVectors;
        pandora::InputBool m_isDigital;
    };

    typedef std::map<std::string, ExtraSubDetectorParameters> ExtraSubDetectorParametersMap;

public:

    DetectorGeometry(JobManager *pJobManager, std::string);
    virtual ~DetectorGeometry() {;}
    
    /**
     * Load geometry from an XML input file produced by GeomConverter in the "pandora" format.
     */
    void loadFromFile(std::string);

    /**
     * Get the map of ExtraSubDetectorParameters.
     */
    ExtraSubDetectorParametersMap* getExtraParameters()
    {
        return &subdetExtras;
    }

    ExtraSubDetectorParameters* getExtraSubDetectorParametersFromType(const std::string& calType)
    {
        return &(subdetExtras[calType]);
    }

    /**
     * Print SubDetectorParameters to cout.
     */
    void printOut(const char* subdetType, PandoraApi::Geometry::SubDetector::Parameters* subdet);

    /**
     * Get the pandora SubDetectorType for a given calorimeter type string.
     */
    pandora::SubDetectorType getPandoraSubDetectorType(const char*) const;

    /**
     * Get the pandora SubDetectorType for a given calorimeter type string.
     */
    pandora::SubDetectorType getPandoraSubDetectorType(const std::string&) const;

    /**
     * Simple utility method to return the InputHitRegion from the calorimeter type.
     */
    inline pandora::InputHitRegion getHitRegion(const std::string& calType) const;

    /**
     * Simple utility method to return the InputHitType from the calorimeter type.
     */
    inline pandora::InputHitType getHitType(const std::string& calType) const;

    /**
     * Get the pointer to the geometry's associated JobManager.
     */
    JobManager* getJobManager() const
    {
        return m_manager;
    }

    /**
     * Get the inner b field strength, units Tesla
     * @return the inner b field strength, units Tesla
     */
    float getInnerBField() const
    {
        return m_innerBField;
    }

    /**
     * Get the detector name.
     * @return The detector name.
     */
    const std::string& getDetectorName() const
    {
        return detectorName;
    }

private:
    JobManager* m_manager;
    ExtraSubDetectorParametersMap subdetExtras;

    float m_innerBField;
    std::string detectorName;
};


#endif
