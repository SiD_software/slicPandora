// $Id: PfoProcessor.h,v 1.4 2010/06/08 22:12:42 jeremy Exp $
#ifndef PFOPROCESSOR_H
#define PFOPROCESSOR_H 1

// slicPandora
#include "EventProcessor.h"

// lcio
#include "EVENT/LCEvent.h"

/**
 * This event processor converts Pandora PFO objects into LCIO output.
 * In the process, it also creates LCIO Cluster's from the Pandora clusters.
 * For neutral particles without a track association, the energy is set correctly
 * in the Pandora PFO.  But for PFOs with associated charged tracks, the energy
 * is not set correctly.  So the associated track is used to set this.
 */
class PfoProcessor : public EventProcessor
{
public:
    PfoProcessor();
    virtual ~PfoProcessor();

public:
    void processEvent(EVENT::LCEvent*);
};

#endif
