#ifndef DummyProcessor_h
#define DummyProcessor_h 1

#include "EventProcessor.h"

class DummyProcessor : public EventProcessor
{
public:
    DummyProcessor();
    virtual ~DummyProcessor();

    void processEvent(EVENT::LCEvent*);
};

#endif
