#ifndef DEFAULTPROCESSORS_H
#define DEFAULTPROCESSORS_H 1

/**
 * List of default slicPandora Processor headers.
 */

// slicPandora
#include "CalorimeterHitProcessor.h"
#include "EventMarkerProcessor.h"
#include "PandoraProcessor.h"
#include "PfoProcessor.h"
#include "ResetPandoraProcessor.h"
#include "SimCalorimeterHitProcessor.h"
#include "SimpleTrackProcessor.h"
#include "MCParticleProcessor.h"

#endif
