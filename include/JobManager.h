// $Id: JobManager.h,v 1.10 2011/09/16 23:31:17 jeremy Exp $
#ifndef JobManager_h
#define JobManager_h 1

// lcio
#include "EVENT/LCEvent.h"

// pandora
#include "Api/PandoraApi.h"

// slicPandora
#include "JobConfig.h"
#include "LcioInputCollectionSettings.h"

class DetectorGeometry;
class EventProcessor;

/**
 * The JobManager is a manager class for running slicPandora jobs.
 * The job configuration is defined in a separate class, JobConfig.
 */
class JobManager
{

public:

    /**
     * A list of EventProcessors.
     */
    typedef std::vector<EventProcessor*> EventProcessors;

    /**
     * Create the JobManager without a configuration.
     */
    JobManager();

    /**
     * Create the JobManager with a configuration.
     */
    JobManager(JobConfig*);

    /**
     * Standard dtor.
     */
    virtual ~JobManager();

    /**
     * Set the job configuration for this run.
     */
    void setJobConfig(JobConfig*);

    /**
     * Get the job configuration for this run.
     */
    JobConfig* getJobConfig();

    /**
     * Run the job with the current settings.
     */
    void run();

    /**
     * Add an EventProcessor to the end of the processor list.
     */
    void addEventProcessor(EventProcessor*);

    /**
     * Get the DetectorGeometry that was created from the input XML file.
     */
    DetectorGeometry* getDetectorGeometry();

    /**
     * Get the Pandora PFA instance.
     */
    const pandora::Pandora& getPandora();   

    /**
     * Get the LCIO input collection settings for the job.
     */
    inline const LcioInputCollectionSettings& getLcioCollectionSettings() const
    {
    	return collectionSettings;
    }

    pandora::StatusCode setupDefaultLcioInputCollectionSettings(LcioInputCollectionSettings& lcioConfig);

private:

    /**
     * Run the processEvent() methods of the EventProcessors in order.
     */
    void processEvent(EVENT::LCEvent*);

    /**
     * Initialize the job so that it is ready to run.
     */
    void initialize();

    /**
     * Create Pandora's algorithm generators.
     */
    pandora::StatusCode registerUserAlgorithmFactories();

    /**
     * Create the geometry from the XML geometry file.
     */
    pandora::StatusCode createGeometry();

private:
    JobConfig* m_config;
    pandora::Pandora* m_pandora;
    DetectorGeometry* m_detectorGeometry;
    EventProcessors m_processors;
    bool m_geometryLoaded;
    bool m_initialized;    
    LcioInputCollectionSettings collectionSettings;
};

#endif
