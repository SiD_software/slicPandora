// $Id: EventProcessor.h,v 1.4 2010/03/11 22:18:44 jeremy Exp $

#ifndef EventProcessor_h
#define EventProcessor_h 1

// stl
#include <string>

// lcio
#include "EVENT/LCEvent.h"

// Avoid circular reference with EventProcessor class.
class JobManager;

using EVENT::LCEvent;

/**
 * This is an API for event processing classes.  Sub-classes must implement
 * the processEvent() method.  Job information can be retrieved by using the 
 * pointer to the JobManager.
 */
class EventProcessor
{
public:

    /**
     * ctor which requires a name argument.
     */
    EventProcessor(std::string name)
        : m_name(name)
    {;}

    /**
     * Standard dtor.
     */
    virtual ~EventProcessor()
    {;}

    /**
     * Set the pointer to the processor's JobManager.
     */
    inline void setJobManager(JobManager* manager)
    {
        m_manager = manager;
    }

    /**
     * Get the pointer to the processor's JobManager.
     */
    inline JobManager* getJobManager() const
    {
        return m_manager;
    }
    
    /**
     * Get the name of the processor.
     */
    inline const std::string& getName() const
    {
        return m_name;
    }

    /**
     * Process a single LCIO event.  This is a pure virtual method that must be 
     * implemented by sub-classes.  Processors that call Pandora algorithms may 
     * ignore the LCEvent and retrieve objects directly from the Pandora instance.
     */
    virtual void processEvent(EVENT::LCEvent*) = 0;

private:
    JobManager* m_manager;
    std::string m_name;
};

#endif
