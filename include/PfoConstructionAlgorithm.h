// $Id: PfoConstructionAlgorithm.h,v 1.6 2012/07/09 16:57:02 grefe Exp $
/**
 *  @file   PandoraPFANew/include/Algorithms/PfoConstructionAlgorithm.h
 * 
 *  @brief  Header file for the pfo construction algorithm class.
 * 
 *  $Log: PfoConstructionAlgorithm.h,v $
 *  Revision 1.6  2012/07/09 16:57:02  grefe
 *  fixed name space for tiny xml classes which changed in pandoraPFA
 *
 *  Revision 1.5  2011/01/31 22:26:42  stanitzk
 *
 *
 *  Changes to make things work with the latest PandoraVersion
 *  Important, remove Extremely_ugly_hack, once GeomConverter produces XML
 *  compliant to John definition
 *
 *  Revision 1.4  2010/11/24 16:44:08  speckmay
 *  status codes are now in namespace pandora
 *
 *  Revision 1.3  2010/06/08 21:43:02  jeremy
 *  make compatible with Pandora release1 (set PANDORA_RELEASE macro to enable); add basic code for digital calorimetry
 *
 *  Revision 1.2  2010/03/09 20:13:42  jeremy
 *  checkpoint
 *
 *  Revision 1.1  2010/01/08 22:55:34  ngraf
 *  First working release.
 *
 */
#ifndef PFO_CONSTRUCTION_ALGORITHM_H
#define PFO_CONSTRUCTION_ALGORITHM_H 1

#include "Pandora/Algorithm.h"

//#include "Algorithms/PfoConstruction/PfoCreationAlgorithm.h"

/**
 *  @brief  PfoConstructionAlgorithm class
 */
class PfoConstructionAlgorithm : public pandora::Algorithm
{
public:
    /**
     *  @brief  Factory class for instantiating algorithm
     */
    class Factory : public pandora::AlgorithmFactory
    {
    public:
        Algorithm *CreateAlgorithm() const;
    };

private:
    pandora::StatusCode Run();
    pandora::StatusCode ReadSettings(const pandora::TiXmlHandle xmlHandle);

    // Member variables here
};

//------------------------------------------------------------------------------------------------------------------------------------------

inline pandora::Algorithm *PfoConstructionAlgorithm::Factory::CreateAlgorithm() const
{
    return new PfoConstructionAlgorithm();
}

#endif // #ifndef PFO_CONSTRUCTION_ALGORITHM_H
