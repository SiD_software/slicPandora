#ifndef LCIOInputCollectionSettings_h
#define LCIOInputCollectionSettings_h 1

#include <string>
#include <map>

#include "Xml/tinyxml.h"
#include "Api/PandoraApi.h"
#include "Pandora/StatusCodes.h"

class LcioInputCollectionSettings
{
	public:

	    /**
	     * This represents the various kinds of calorimeters within Pandora.
	     */
    	enum CaloType { UNKNOWN, EM_BARREL, EM_ENDCAP, HAD_BARREL, HAD_ENDCAP, MUON_BARREL, MUON_ENDCAP };

    	/**
    	 * Map of CaloType to list of collections.
    	 */
    	typedef std::map<CaloType, std::vector<std::string> > CaloCollectionMap;

	    LcioInputCollectionSettings();
	    virtual ~LcioInputCollectionSettings();

	    /**
	     * Read LCIO collection settings from an XML file.
	     */
    	pandora::StatusCode readSettings(const std::string& xmlFileName);

    	/**
    	 * Add a link from CaloType to a collection.
    	 */
    	void addCaloCollection(CaloType type, const std::string& collection)
    	{
    	    caloCollectionMap[type].push_back(std::string(collection));
    	}

    	/**
    	 * Get the name of the Track collection.
    	 */
    	const std::string& getTrackCollectionName() const
    	{
    	    return trackCollectionName;
    	}

    	/**
    	 * Get the name of the TrackState collection.
    	 */
    	inline std::string getTrackStateCollectionName(const std::string& trackState)
    	{
    	    return trackStateMap[trackState];
    	}

    	/**
    	 * Was setup from XML?
    	 */
    	inline bool initialized() const
    	{
    		return m_initialized;
    	}

    	/**
    	 * Get the CaloType from a string.
    	 */
    	static const CaloType getTypeFromString(const std::string& caloTypeName);

    	/**
    	 * Get the type string from a CaloType.
    	 */
    	static const std::string& getStringFromType(CaloType caloType);

    	/**
    	 * Get the map of CalorimeterHit collection names.
    	 */
    	const CaloCollectionMap& getCaloCollectionMap() const
    	{
    	    return caloCollectionMap;
    	}

    	/**
    	 * Get the list of default CalorimeterHit subdetector types.
    	 */
    	const std::vector<std::string>& getDefaultCalorimeterTypes() const
    	{
    	    return defaultCaloTypes;
    	}

	private:

    	bool m_initialized;

    	std::vector<std::string> defaultCaloTypes;
    	CaloCollectionMap caloCollectionMap;
    	std::string trackCollectionName;
    	std::map<std::string, std::string> trackStateMap;

        static std::string emBarrel;
        static std::string emEndcap;
        static std::string hadBarrel;
        static std::string hadEndcap;
        static std::string muonBarrel;
        static std::string muonEndcap;
        static std::string unknown;
};

#endif
