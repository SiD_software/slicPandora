// $Id: PfoProcessor.cpp,v 1.19 2012/01/31 15:08:34 jeremy Exp $
#include "PfoProcessor.h"

// lcio

#include "EVENT/LCCollection.h"
#include "EVENT/SimCalorimeterHit.h"
#include "EVENT/CalorimeterHit.h"
#include "IMPL/LCCollectionVec.h"
#include "IMPL/ReconstructedParticleImpl.h"
#include "IMPL/ClusterImpl.h"
#include "IMPL/LCFlagImpl.h" 
#include "EVENT/LCIO.h"
#include "EVENT/Track.h"

// slicPandora
#include "PfoConstructionAlgorithm.h"
#include "JobManager.h"
#include "ClusterShapes.h"
#include "DetectorGeometry.h"

// Pandora
#include "Managers/PluginManager.h"
#include "Objects/CartesianVector.h"
#include "Objects/ParticleFlowObject.h"
#include "Plugins/BFieldPlugin.h"

using IMPL::LCCollectionVec;
using IMPL::LCFlagImpl;
using IMPL::ClusterImpl;
using IMPL::ReconstructedParticleImpl;
using EVENT::CalorimeterHit;
using EVENT::Track;
using std::cout;
using std::endl;

PfoProcessor::PfoProcessor()
    : EventProcessor("PfoProcessor")
{}

PfoProcessor::~PfoProcessor()
{}

void PfoProcessor::processEvent(EVENT::LCEvent* event)
{    
	// Set flag whether to delete existing collections.
	bool deleteExistingCollections = getJobManager()->getJobConfig()->deleteExistingCollections();

    // Make a container for the clusters.
    LCCollectionVec* clusterVec = new LCCollectionVec(EVENT::LCIO::CLUSTER);

    // Set flag for pointing back to CalorimeterHits.
    LCFlagImpl clusterFlag(0);
    clusterFlag.setBit(EVENT::LCIO::CLBIT_HITS);
    clusterVec->setFlag(clusterFlag.getFlag());

    // Get Pandora's list of PFOs.
    const pandora::PfoList *pfoList = NULL;
    PandoraApi::GetCurrentPfoList(getJobManager()->getPandora(), pfoList);

    // Make a container for the ReconstructedParticles.
    LCCollectionVec* pReconstructedParticleCollection = new LCCollectionVec(EVENT::LCIO::RECONSTRUCTEDPARTICLE);

    // Iterate over Pandora's PFO objects to create LCIO ReconstructedParticles.
    for (pandora::PfoList::const_iterator itPFO = pfoList->begin(), itPFOEnd = pfoList->end(); itPFO != itPFOEnd; ++itPFO)
    {
#ifdef PFOPROCESSOR_DEBUG
        std::cout << std::endl << "proc PFO: " << (int)(&(*itPFO)) << std::endl;
#endif

        // Make the new ReconstructedParticle.
        ReconstructedParticleImpl *pReconstructedParticle = new ReconstructedParticleImpl();

        pandora::ClusterAddressList clusterAddressList = (*itPFO)->GetClusterAddressList();
#ifdef PFOPROCESSOR_DEBUG
        cout << "Pandora found <" << clusterAddressList.size() << "> clusters." << endl;
#endif
        pandora::TrackAddressList trackAddressList = (*itPFO)->GetTrackAddressList();

#ifdef PFOPROCESSOR_DEBUG
        cout << "Pandora found <" << trackAddressList.size() << "> tracks." << std::endl;
#endif

        // Iterate over the cluster list and make LCIO clusters.
        for (pandora::ClusterAddressList::iterator itCluster = clusterAddressList.begin(), itClusterEnd = clusterAddressList.end();
             itCluster != itClusterEnd; ++itCluster)
        {
            // Make a new Cluster.
            ClusterImpl *pCluster = new ClusterImpl();
            
            const unsigned int nHitsInCluster((*itCluster).size());

            float clusterEnergy(0.);
            float *pHitE = new float[nHitsInCluster];
            float *pHitX = new float[nHitsInCluster];
            float *pHitY = new float[nHitsInCluster];
            float *pHitZ = new float[nHitsInCluster];

            for (unsigned int iHit = 0; iHit < nHitsInCluster; ++iHit)
            {
                CalorimeterHit *pCalorimeterHit = (CalorimeterHit*)((*itCluster)[iHit]);
                pCluster->addHit(pCalorimeterHit, 1.0);

                const float caloHitEnergy(pCalorimeterHit->getEnergy());
                clusterEnergy += caloHitEnergy;

                pHitE[iHit] = caloHitEnergy;
                pHitX[iHit] = pCalorimeterHit->getPosition()[0];
                pHitY[iHit] = pCalorimeterHit->getPosition()[1];
                pHitZ[iHit] = pCalorimeterHit->getPosition()[2];              
            }

            pCluster->setEnergy(clusterEnergy);

            ClusterShapes *pClusterShapes = new ClusterShapes(nHitsInCluster, pHitE, pHitX, pHitY, pHitZ);
            pCluster->setPosition(pClusterShapes->getCentreOfGravity());

            // TODO Set IPhi and ITheta here.

#ifdef PFOPROCESSOR_DEBUG
            cout << "Cluster contains <" << pCluster->getCalorimeterHits().size() << "> hits." << endl;
#endif

            // Add the cluster to the collection.
            clusterVec->addElement(pCluster);

            // Associate the cluster with the ReconstructedParticle.
            pReconstructedParticle->addCluster(pCluster);

            delete pClusterShapes;
            delete[] pHitE; delete[] pHitX; delete[] pHitY; delete[] pHitZ;
        }

        // Set the ReconstructedParticle parameters from the PFO.
#ifdef PFOPROCESSOR_DEBUG
        std::cout << "Pandora PFO momentum " << (*itPFO)->GetMomentum().GetX()<< " " << (*itPFO)->GetMomentum().GetY()<< " " << (*itPFO)->GetMomentum().GetZ() << std::endl;
        std::cout << "Pandora PFO energy " << (*itPFO)->GetEnergy() << std::endl;
#endif
        float momentum[3] = {(*itPFO)->GetMomentum().GetX(), (*itPFO)->GetMomentum().GetY(), (*itPFO)->GetMomentum().GetZ()};
        pReconstructedParticle->setMomentum(momentum);
        pReconstructedParticle->setEnergy((*itPFO)->GetEnergy());
        pReconstructedParticle->setMass((*itPFO)->GetMass());
        pReconstructedParticle->setCharge((*itPFO)->GetCharge());
        pReconstructedParticle->setType((*itPFO)->GetParticleId());

        // Temporary variables to access the track momentum.
        // commented out as we don't need these anymore  new Pandora version[D
//        DetectorGeometry* detector = getJobManager()->getDetectorGeometry();
//        PandoraApi::Geometry::Parameters* pandoraGeomParams = detector->getGeometryParameters();
//        double magneticField =  pandoraGeomParams->m_bField.Get(CartesianVector(0.f,0.f,0.f));
//        double magneticField = pGeometryHelper->GetBField(pandora::CartesianVector(0.f,0.f,0.f));
        const pandora::Pandora& pandora = getJobManager()->getPandora();
        const float magneticField(pandora.GetPlugins()->GetBFieldPlugin()->GetBField(pandora::CartesianVector(0.f,0.f,0.f)));

#ifdef PFOPROCESSOR_DEBUG
        std::cout << "BField= " << magneticField << std::endl;
#endif
        double fieldConversion = 2.99792458e-4;
        double px = 0.;
        double py = 0.;
        double pz = 0.;
        double pT = 0.;
        double energy = 0.;
#ifdef PFOPROCESSOR_DEBUG
        if (trackAddressList.size()>1) std::cout  << " PFO has "<< trackAddressList.size() " tracks." << std::endl;
#endif
        // Associate the Tracks with the ReconstructedParticles.
        for (pandora::TrackAddressList::iterator itTrack = trackAddressList.begin(), itTrackEnd = trackAddressList.end(); 
             itTrack != itTrackEnd;
             ++itTrack)
        {
#ifdef PFOPROCESSOR_DEBUG
            std::cout << "Adding track to RP." << std::endl;
#endif

            // Compute track momentum and energy from LCIO Track parameters.
            Track* t = (Track*) (*itTrack);
            double omega = t->getOmega();
            double phi = t->getPhi();
            double tanLambda = t->getTanLambda();
            pT = fabs(1./omega) * magneticField * fieldConversion;
            px = pT * cos(phi);
            py = pT * sin(phi);
            pz = pT * tanLambda;
            energy = sqrt(px * px + py * py + pz * pz + (*itPFO)->GetMass() * (*itPFO)->GetMass());

            momentum[0] = px;
            momentum[1] = py;
            momentum[2] = pz;

#ifdef PFOPROCESSOR_DEBUG
            std::cout << "    track (px, py, pz) : (" << px << ", " << py << ", " << pz << ")" << std::endl;
#endif

            // Add the Track to the output LCIO ReconstructedParticle.
            pReconstructedParticle->addTrack(t);
        }

        // PFOs with tracks do not have either their momentum or their energy correctly set.  
        // Override with information from the track.
        // TODO Understand why this is happening.
        // FIXME What happens in the case of > 1 Track?  Can this happen at all?
        if(trackAddressList.size() !=0)
        {
            pReconstructedParticle->setMomentum(momentum);
            pReconstructedParticle->setEnergy(energy);
        }

        // Add the ReconstructedParticle to the collection.
        pReconstructedParticleCollection->addElement(pReconstructedParticle);
    }
    
    // Add the list of clusters to the event.
    if (deleteExistingCollections)
    {
    	event->removeCollection("ReconClusters");
    }
    event->addCollection(clusterVec, "ReconClusters");

    // Add the list of ReconstructedParticles to the event.
    if (deleteExistingCollections)
    {
    	event->removeCollection("PandoraPFOCollection");
    }
    event->addCollection(pReconstructedParticleCollection, "PandoraPFOCollection");       
}
