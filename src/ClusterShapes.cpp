/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

#include "ClusterShapes.h"



// #################################################
// #####                                       #####
// #####  Additional Structures and Functions  #####
// #####                                       #####
// #################################################
//=============================================================================

struct data {
  int n;
  float* x;
  float* y;
  float* z;
  float* ex;
  float* ey;
  float* ez;
};


// ##########################################
// #####                                #####
// #####   Constructor and Destructor   #####
// #####                                #####
// ##########################################

//=============================================================================

ClusterShapes::ClusterShapes(int nhits, float* a, float* x, float* y, float* z){

  _nHits = nhits;
  _aHit = new float[_nHits];
  _xHit = new float[_nHits];
  _yHit = new float[_nHits];
  _zHit = new float[_nHits];
  _exHit = new float[_nHits];   
  _eyHit = new float[_nHits];
  _ezHit = new float[_nHits];         
  _types = new int[_nHits];

  _xl = new float[_nHits];
  _xt = new float[_nHits];

  _t = new float[_nHits];
  _s = new float[_nHits]; 

  for (int i(0); i < nhits; ++i) {
    _aHit[i] = a[i];
    _xHit[i] = x[i];
    _yHit[i] = y[i];
    _zHit[i] = z[i];
    _exHit[i] = 1.0;
    _eyHit[i] = 1.0;
    _ezHit[i] = 1.0;
    _types[i] = 1; // all hits are assumed to be "cyllindrical"
  }

  _ifNotGravity     = 1;
  _ifNotWidth       = 1;
  _ifNotInertia     = 1;
  _ifNotEigensystem = 1;

}


//=============================================================================

ClusterShapes::~ClusterShapes() {

  delete[] _aHit;
  delete[] _xHit;
  delete[] _yHit;
  delete[] _zHit;
  delete[] _exHit;
  delete[] _eyHit;
  delete[] _ezHit;
  delete[] _types;
  delete[] _xl;
  delete[] _xt;
  delete[] _t;
  delete[] _s;
}

//=============================================================================

float* ClusterShapes::getCentreOfGravity() {
  if (_ifNotGravity == 1) findGravity() ;
  return &_analogGravity[0] ;
}

//=============================================================================

void ClusterShapes::findGravity() 
{
    _totAmpl = 0. ;
    for (int i(0); i < 3; ++i) 
    {
      _analogGravity[i] = 0.0 ;
    }
    for (int i(0); i < _nHits; ++i) {
      _totAmpl+=_aHit[i] ;
      _analogGravity[0]+=_aHit[i]*_xHit[i] ;
      _analogGravity[1]+=_aHit[i]*_yHit[i] ;
      _analogGravity[2]+=_aHit[i]*_zHit[i] ;
    }
    for (int i(0); i < 3; ++i) {
      _analogGravity[i]/=_totAmpl ;
    }
    _xgr = _analogGravity[0];
    _ygr = _analogGravity[1];
    _zgr = _analogGravity[2];
    _ifNotGravity = 0;
}

