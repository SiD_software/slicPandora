#include "JobManager.h"

// slicPandora
#include "JobConfig.h"
#include "DetectorGeometry.h"
#include "EventProcessor.h"

// pandora
#include "LCContent.h"
#include "PfoConstructionAlgorithm.h"

// lcio
#include "IOIMPL/LCFactory.h"
#include "EVENT/LCIO.h"

// stl
#include <iostream>

using namespace std;
using IO::LCReader;
using IO::LCWriter;
using IOIMPL::LCFactory;

JobManager::JobManager()
    : m_config(0),
      m_pandora(0),
      m_detectorGeometry(0),
      m_geometryLoaded(false)
{}

JobManager::JobManager(JobConfig* config)
    : m_config(config),
      m_pandora(0),
      m_detectorGeometry(0),
      m_geometryLoaded(false)
{}

JobManager::~JobManager()
{}

pandora::StatusCode JobManager::setupDefaultLcioInputCollectionSettings(LcioInputCollectionSettings& lcioConfig)
{
    const std::vector<std::string>& caloTypes = lcioConfig.getDefaultCalorimeterTypes();

    for (std::vector<std::string>::const_iterator it = caloTypes.begin(); it != caloTypes.end(); it++)
    {
        // Get the CaloType string.
        const std::string& caloType = (*it);

        // Get the collection name from the CaloType.
        const std::string& caloColl = this->getDetectorGeometry()->getExtraSubDetectorParametersFromType(caloType)->m_collection;

        // Add link from CaloType to collection.
        lcioConfig.addCaloCollection(LcioInputCollectionSettings::getTypeFromString(caloType), caloColl);

        // DEBUG
        std::cout << "added default mapping of CaloType " << caloType << " to collection " << caloColl << std::endl;
    }

    return pandora::STATUS_CODE_SUCCESS;
}

void JobManager::initialize()
{
    // Create new Pandora instance.    
    m_pandora = new pandora::Pandora();
    
    // Create the slicPandora DetectorGeometry.
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, createGeometry());

    // Read in the LCIO input collection settings which may override the defaults from the detector.
    if (!m_config->useDefaultCaloTypes())
    {
        //std::cout << "using custom LCIO input config" << std::endl; // DEBUG
        PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, collectionSettings.readSettings(m_config->getLcioConfigFile()));
    }
    // Use default settings of readout collections from the geometry XML file.
    else
    {
        //std::cout << "default CalTypes" << std::endl; // DEBUG
        PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, setupDefaultLcioInputCollectionSettings(collectionSettings));
    }

    // FineGranuality library content
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, LCContent::RegisterAlgorithms(*m_pandora));
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, LCContent::RegisterBasicPlugins(*m_pandora));
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, LCContent::RegisterBFieldPlugin(*m_pandora, getDetectorGeometry()->getInnerBField(), -1.5f, 0.01f)); // FIXME: get all bfield parameters from config

    // Register the the user algorithm factories with Pandora.
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, registerUserAlgorithmFactories());

    // Read the run control settings into Pandora.
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::ReadSettings(*m_pandora, m_config->getPandoraSettingsXmlFile()));
}

void JobManager::setJobConfig(JobConfig* config)
{
    m_config = config;
}

JobConfig* JobManager::getJobConfig()
{
    return m_config;
}

void JobManager::addEventProcessor(EventProcessor* processor)
{
    processor->setJobManager(this);
    m_processors.push_back(processor);
}

pandora::StatusCode JobManager::registerUserAlgorithmFactories()
{
    pandora::AlgorithmFactory* pfac = new PfoConstructionAlgorithm::Factory();
    PandoraApi::RegisterAlgorithmFactory(*m_pandora, "PFO_Construction", pfac);
    return pandora::STATUS_CODE_SUCCESS;
}

pandora::StatusCode JobManager::createGeometry()
{
    m_detectorGeometry = new DetectorGeometry(this, m_config->getGeometryFile());
    m_geometryLoaded = true;

    return pandora::STATUS_CODE_SUCCESS;
}

DetectorGeometry* JobManager::getDetectorGeometry()
{
    return m_detectorGeometry;
}

void JobManager::run()
{
    std::cout << " GEO DERP \n";
    initialize();

    LCReader* reader = LCFactory::getInstance()->createLCReader();
    LCWriter* writer = LCFactory::getInstance()->createLCWriter();
   
    writer->setCompressionLevel(9);
    writer->open(m_config->getOutputFile().c_str(), EVENT::LCIO::WRITE_NEW);

    // Open file list.
    JobConfig::FileList inputFiles = m_config->getInputFiles();
    
    reader->open(m_config->getInputFiles());

    // Skip events.
    if (m_config->getSkipEvents() > 0)
        reader->skipNEvents(m_config->getSkipEvents());
    
    // Event counter.
    int nread = 0;

    // Read first event.
    LCEvent* event = reader->readNextEvent(EVENT::LCIO::UPDATE);

    // Check for first event and bail if did not get one.
    if (0 == event)
    {
        std::cout << "Failed to read first event!!!" << std::endl;
        exit(1);
    }

    ++nread;
    int ntoread = m_config->getNumberOfEvents();

    // Process input LCIO events.
    while (event != 0)
    {        
        // Run event processors over this event.
        processEvent(event);

        // Write out event to recon LCIO file.
        writer->writeEvent(event);

        // Read the next event.
        event = reader->readNextEvent(EVENT::LCIO::UPDATE);
 
        // End of input file.
        if (0 == event)
        {
            std::cout << std::endl << "Ran out of events after <" << nread << "> events!" << std::endl;
            break;
        }

        // Check if job should be stopped due to user-specified event limit.
        if ((ntoread != -1) && (nread >= ntoread))
        {
            std::cout << std::endl << "Stopping run after <" << ntoread << "> event limit!" << std::endl;
            break;
        }

        // Increment number of events read.
        ++nread;
    }
    
    // Close the LCIO reader.
    reader->close();

    // Close the LCIO writer.
    writer->flush();
    writer->close();

    // End of job printout.
    std::cout << "Pandora ran <" << nread << "> events in this job." << std::endl;

    delete m_pandora;
}

void JobManager::processEvent(EVENT::LCEvent* event)
{
    for (EventProcessors::iterator it = m_processors.begin();
         it != m_processors.end();
         it++)
    {
        std::cout << "JobManager::processEvent - calling processor: " << (*it)->getName() << std::endl;
        (*it)->processEvent(event);
    }
}

const pandora::Pandora& JobManager::getPandora()
{
    return const_cast<const pandora::Pandora&> (*m_pandora);
}
