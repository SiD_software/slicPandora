// $Id: LcioInputCollectionSettings.cpp,v 1.4 2012/07/09 16:57:02 grefe Exp $
#include "LcioInputCollectionSettings.h"

std::string LcioInputCollectionSettings::emBarrel = "EM_BARREL";
std::string LcioInputCollectionSettings::emEndcap = "EM_ENDCAP";
std::string LcioInputCollectionSettings::hadBarrel = "HAD_BARREL";
std::string LcioInputCollectionSettings::hadEndcap = "HAD_ENDCAP";
std::string LcioInputCollectionSettings::muonBarrel = "MUON_BARREL";
std::string LcioInputCollectionSettings::muonEndcap = "MUON_ENDCAP";
std::string LcioInputCollectionSettings::unknown = "UNKNOWN";

LcioInputCollectionSettings::LcioInputCollectionSettings()
	: m_initialized(false),
	  trackCollectionName("Tracks")
{
    // Default list of hit collection types to read in.
    defaultCaloTypes.push_back(emBarrel);
    defaultCaloTypes.push_back(emEndcap);
    defaultCaloTypes.push_back(hadBarrel);
    defaultCaloTypes.push_back(hadEndcap);
    defaultCaloTypes.push_back(muonBarrel);
    defaultCaloTypes.push_back(muonEndcap);

    // Default names of TrackState collections.
    trackStateMap["StateAtStart"] = "StateAtStart";
    trackStateMap["StateAtECal"] = "StateAtECal";
    trackStateMap["StateAtEnd"] = "StateAtEnd";
}

LcioInputCollectionSettings::~LcioInputCollectionSettings()
{;}

pandora::StatusCode LcioInputCollectionSettings::readSettings(const std::string& xmlFileName)
{
    std::cout << "Reading in LCIO collection settings from XML file " << xmlFileName << " ... " << std::endl << std::endl;

	if (m_initialized)
	{
		std::cout << "Already initialized." << std::endl;
		return pandora::STATUS_CODE_FAILURE;
	}

	try
	{
		pandora::TiXmlDocument xmlDoc(xmlFileName);

		if (!xmlDoc.LoadFile())
		{
			std::cout << "LCIOInputCollectionSettings::readSettings - Invalid XML file." << std::endl;
			throw pandora::StatusCodeException(pandora::STATUS_CODE_FAILURE);
		}

		const pandora::TiXmlHandle pXmlDocHandle(&xmlDoc);
		const pandora::TiXmlHandle pXmlHandle = pandora::TiXmlHandle(pXmlDocHandle.FirstChildElement().Element());

		// Loop over CaloCollection elements.
		std::cout << "Using these CalorimeterHit collections ..." << std::endl;
		for (pandora::TiXmlElement* pXmlElement = pXmlHandle.FirstChild("CaloCollection").Element(); NULL != pXmlElement;
				pXmlElement = pXmlElement->NextSiblingElement("CaloCollection"))
		{
		     std::cout << pXmlElement->Attribute("name") << " with type " << pXmlElement->Attribute("caloType") << std::endl;

			CaloType calType = getTypeFromString(pXmlElement->Attribute("caloType"));
			std::string collName = pXmlElement->Attribute("name");
			if (calType != UNKNOWN)
			{
				caloCollectionMap[calType].push_back(std::string(collName));
			}
			else
			{
				std::cout << "Unknown caloType argument." << std::endl;
				return pandora::STATUS_CODE_FAILURE;
			}
		}
		std::cout << std::endl;

		// Track collection.
		pandora::TiXmlElement* pTrackCollElement = pXmlHandle.FirstChild("TrackCollection").Element();
		if (pTrackCollElement != NULL)
		{
		    std::cout << "Using this TrackCollection ..." << std::endl;
			if (pTrackCollElement->Attribute("name") != NULL)
			{
			    std::cout << pTrackCollElement->Attribute("name") << std::endl;
				trackCollectionName = pTrackCollElement->Attribute("name");
			}
			else
			{
				std::cout << "TrackCollection is missing name attribute." << std::endl;
				return pandora::STATUS_CODE_FAILURE;
			}
		}
		std::cout << std::endl;

		// TrackState setup.
		std::cout << "Checking for custom TrackState collection settings ..." << std::endl;
		for (pandora::TiXmlElement* pXmlElement = pXmlHandle.FirstChild("TrackStateCollection").Element(); NULL != pXmlElement;
		                pXmlElement = pXmlElement->NextSiblingElement("TrackStateCollection"))
		{
		    if (pXmlElement->Attribute("name") == NULL)
		    {
		        std::cout << "TrackStateCollection is missing name attribute." << std::endl;
		        return pandora::STATUS_CODE_FAILURE;
		    }
		    if (pXmlElement->Attribute("trackState") == NULL)
		    {
		        std::cout << "TrackStateCollection is missing trackState attribute." << std::endl;
		        return pandora::STATUS_CODE_FAILURE;
		    }
		    std::string name = pXmlElement->Attribute("name");
		    std::string trackState = pXmlElement->Attribute("trackState");
		    trackStateMap[trackState] = name;
		    std::cout << name << " with state " << trackState << std::endl;
		}
		std::cout << std::endl;
	}
	catch (pandora::StatusCodeException &statusCodeException)
	{
		std::cout << "Failure in reading collection settings." << statusCodeException.ToString() << std::endl;
		return pandora::STATUS_CODE_FAILURE;
	}
	catch (...)
	{
		std::cout << "Unrecognized exception while reading LCIO collection settings." << std::endl;
		return pandora::STATUS_CODE_FAILURE;
	}

	m_initialized = true;

	return pandora::STATUS_CODE_SUCCESS;

}

const LcioInputCollectionSettings::CaloType LcioInputCollectionSettings::getTypeFromString(const std::string& caloTypeName)
{
	if (caloTypeName.compare("EM_BARREL") == 0)
	{
		return EM_BARREL;
	}
	else if (caloTypeName.compare("EM_ENDCAP") == 0)
	{
		return EM_ENDCAP;
	}
	else if (caloTypeName.compare("HAD_BARREL") == 0)
	{
		return HAD_BARREL;
	}
	else if (caloTypeName.compare("HAD_ENDCAP") == 0)
	{
		return HAD_ENDCAP;
	}
	else if (caloTypeName.compare("MUON_BARREL") == 0)
	{
		return MUON_BARREL;
	}
	else if (caloTypeName.compare("MUON_ENDCAP") == 0)
	{
		return MUON_ENDCAP;
	}
	else
	{
		return UNKNOWN;
	}
}

const std::string& LcioInputCollectionSettings::getStringFromType(CaloType caloType)
{
    if (caloType == EM_BARREL)
    {
        return emBarrel;
    }
    else if (caloType == EM_ENDCAP)
    {
        return emEndcap;
    }
    else if (caloType == HAD_BARREL)
    {
        return hadBarrel;
    }
    else if (caloType == HAD_ENDCAP)
    {
        return hadEndcap;
    }
    else if (caloType == MUON_BARREL)
    {
        return muonBarrel;
    }
    else if (caloType == MUON_ENDCAP)
    {
        return muonEndcap;
    }
    else
    {
        return unknown;
    }
}
