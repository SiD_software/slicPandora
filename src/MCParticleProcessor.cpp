// $Id: MCParticleProcessor.cpp,v 1.4 2013/01/16 17:52:43 jeremy Exp $

#include "MCParticleProcessor.h"

// lcio
#include "EVENT/LCCollection.h"

// slicPandora
#include "IDDecoder.h"
#include "JobManager.h"
#include "JobConfig.h"
#include "DetectorGeometry.h"

// stl
#include <cmath>

using std::fabs;
using EVENT::MCParticle;
using EVENT::LCCollection;

std::string MCParticleProcessor::mcParticleCollectionName = "MCParticle";

void MCParticleProcessor::processEvent(EVENT::LCEvent* event)
{
    //const pandora::Pandora& pandora = getJobManager()->getPandora();

    const LCCollection* mcParticles;

    // Check again if collection exists.  If not, could be okay so skip and move on.
    try 
    {
        mcParticles = event->getCollection(mcParticleCollectionName);
    }
    catch (...)
    {
        std::cout << "No MCParticle collection '" << mcParticleCollectionName << "' found. " << std::endl;
        return;
    }

    int nptcl = mcParticles->getNumberOfElements();

    for (int i=0; i<nptcl; i++)
    {
        MCParticle* mcParticle = dynamic_cast<MCParticle*> (mcParticles->getElementAt(i));
        //std::cout << "creating mcparticle with id: " << i << std::endl;
        PandoraApi::MCParticle::Parameters mcParticleParams = makeMCParticleParameters(mcParticle, i);

#ifdef MCPARTICLE_PARAMS_DEBUG
        printCaloHitParameters(mcParticleParams);
#endif
    }
#ifdef MCPARTICLE_PARAMS_DEBUG
    std::cout << "-----------------------------------------------------" << std::endl;
#endif
}


PandoraApi::MCParticle::Parameters MCParticleProcessor::makeMCParticleParameters(MCParticle* pMcParticle, int id)
{
    const pandora::Pandora& pandora = getJobManager()->getPandora();

    PandoraApi::MCParticle::Parameters mcParticleParameters;
    mcParticleParameters.m_mcParticleType = pandora::MC_3D;
    mcParticleParameters.m_particleId = id;
    mcParticleParameters.m_energy = pMcParticle->getEnergy();
    mcParticleParameters.m_particleId = pMcParticle->getPDG();
    mcParticleParameters.m_pParentAddress = pMcParticle;
    mcParticleParameters.m_momentum = pandora::CartesianVector(pMcParticle->getMomentum()[0], pMcParticle->getMomentum()[1],
                                                               pMcParticle->getMomentum()[2]);
    mcParticleParameters.m_vertex = pandora::CartesianVector(pMcParticle->getVertex()[0], pMcParticle->getVertex()[1],
                                                             pMcParticle->getVertex()[2]);
    mcParticleParameters.m_endpoint = pandora::CartesianVector(pMcParticle->getEndpoint()[0], pMcParticle->getEndpoint()[1],
                                                               pMcParticle->getEndpoint()[2]);

    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::MCParticle::Create(pandora, mcParticleParameters));

    // Create parent-daughter relationships
    for(std::vector<MCParticle*>::const_iterator itDaughter = pMcParticle->getDaughters().begin(),
            itDaughterEnd = pMcParticle->getDaughters().end(); itDaughter != itDaughterEnd; ++itDaughter)
    {
        PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::SetMCParentDaughterRelationship(pandora, pMcParticle, *itDaughter));
    }

    return mcParticleParameters;                                               
}


void MCParticleProcessor::printMCParticleParameters(const PandoraApi::MCParticle::Parameters& params)
{
    std::cout << params.m_vertex.Get() << " [mm]" << std::endl;
    std::cout << params.m_endpoint.Get() << " [mm]" << std::endl;
    std::cout << "    energy: " << params.m_energy.Get() << " [GeV]" << std::endl;
    std::cout << params.m_momentum.Get() << " [GeV/c]" << std::endl;
    std::cout << "    particleID: " << params.m_particleId.Get() << " " << std::endl;
}
