#include "PandoraProcessor.h"

// slicPandora
#include "JobManager.h"

// pandora
#include "Pandora/StatusCodes.h"
#include "Api/PandoraApi.h"

void PandoraProcessor::processEvent(EVENT::LCEvent*)
{   
    //std::cout << "PandoraProcessor::processEvent" << std::endl;
    pandora::StatusCode stat = PandoraApi::ProcessEvent(getJobManager()->getPandora());
    if (stat != pandora::STATUS_CODE_SUCCESS)
    {
        std::cerr << "PandoraApi::ProcessEvent did not succeed!" << std::endl;
        exit(1);
    }
}
