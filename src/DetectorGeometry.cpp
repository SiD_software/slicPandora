#include "DetectorGeometry.h"

// pandora
#include "Pandora/PandoraInputTypes.h"
#include "Pandora/ObjectCreation.h"
#include "Helpers/XmlHelper.h"

// slicPandora
#include "IDDecoder.h"
#include "JobManager.h"

// stl
#include <stdexcept>
#include <cstdio>
#include <cstring>
#include <cmath>

using namespace std;

// Remove comment for debug output.
#define DETECTOR_GEOMETRY_DEBUG 1

DetectorGeometry::DetectorGeometry(JobManager *pJobManager, std::string filename) :
    m_manager(pJobManager),
    m_innerBField(5.f)
{
    loadFromFile(filename);
}

void DetectorGeometry::loadFromFile(std::string filename)
{
    // Load doc and check if valid.
    pandora::TiXmlDocument doc(filename.c_str());
    if (!doc.LoadFile())
    {
        printf("line %i:::%s\n", doc.ErrorRow(), doc.ErrorDesc());
        throw runtime_error("Parse error reading input XML file.");
    }

    // Get the root element.
    pandora::TiXmlElement* root = doc.RootElement();

    // Get the name of the detector.
    pandora::TiXmlElement* detector = root->FirstChildElement("detector");
    if (detector->Attribute("name") != NULL)
    {
        detectorName = detector->Attribute("name");
    }
    else
    {
        throw runtime_error("Missing name of detector in XML geometry file.");
    }
    std::cout << " loading Detector   \n";

    const pandora::Pandora& pandora = getJobManager()->getPandora();

    // Process the calorimeter elements.
    pandora::TiXmlElement* calorimeters = root->FirstChildElement("calorimeters");
    pandora::TiXmlElement* calElem = (pandora::TiXmlElement*) calorimeters->FirstChild("calorimeter");
    //for ( ;
    //      calElem;
    do {
        // Get the type of calorimeter.
        const char* subdetType = calElem->Attribute("type");
        std::string subdetTypeStr(subdetType);
        
        // Pick the right subdetector object to populate.
        const pandora::SubDetectorType subDetectorType(this->getPandoraSubDetectorType(subdetType));

        // Check if subdetector type is known and skip if not.
        if (pandora::SUB_DETECTOR_OTHER == subDetectorType)
        {
            std::cerr << "WARNING: Ignoring unknown subdetType " << subdetType << "." << std::endl;
            continue;
        }

        // Extra parameters defined by the GeomConverter format.
        PandoraApi::Geometry::SubDetector::Parameters subdet;
        ExtraSubDetectorParameters extras;

        subdet.m_subDetectorType = subDetectorType;
        subdet.m_subDetectorName = subdetType;

        // Numerical parameters which are attributes on calorimeter.
        float innerR, innerZ, innerPhi;
        innerR = innerZ = innerPhi = 0;
        int innerSym = 0;
        float outerR, outerZ, outerPhi; 
        outerR = outerZ = outerPhi = 0;
        int outerSym = 0;
        int nlayers = 0;
        float mipEnergy = 0;
        float mipSigma = 0;
        float timeCut = 0;
        float mipCut = 0;

        // If inner and outer Z are both negative, then flip them. --JM    
        if (innerZ < 0 && outerZ < 0)
        {
            innerZ = -innerZ;
            outerZ = -outerZ;
        }

        // Read in parameters.
        calElem->QueryFloatAttribute("innerR", &innerR );        
        calElem->QueryFloatAttribute("innerZ", &innerZ );        
        calElem->QueryFloatAttribute("innerPhi", &innerPhi );
        calElem->QueryIntAttribute("innerSymmetryOrder", &innerSym );
        calElem->QueryFloatAttribute("outerR", &outerR );        
        calElem->QueryFloatAttribute("outerZ", &outerZ );        
        calElem->QueryFloatAttribute("outerPhi", &outerPhi );
        calElem->QueryIntAttribute("outerSymmetryOrder", &outerSym );
        calElem->QueryFloatAttribute("mipEnergy", &mipEnergy );
        calElem->QueryFloatAttribute("mipCut", &mipCut );
        calElem->QueryFloatAttribute("mipSigma", &mipSigma );
        calElem->QueryFloatAttribute("timeCut", &timeCut );

        // Set subdetector parameters.
        subdet.m_innerRCoordinate = innerR;
        subdet.m_innerZCoordinate = innerZ;
        subdet.m_innerPhiCoordinate = innerPhi;
        subdet.m_innerSymmetryOrder = innerSym;
        subdet.m_outerRCoordinate = outerR;
        subdet.m_outerZCoordinate = outerZ;
        subdet.m_outerPhiCoordinate = outerPhi;
        subdet.m_outerSymmetryOrder = outerSym;

        // By default, all components should be mirrored in Z (don't have to, but then may need to create separate e.g. left and right sub detectors)
        subdet.m_isMirroredInZ = true;	
	
        // Inner and outer symmetry are different.
        if (innerSym != outerSym)
        {
            std::cerr << "Inner and outer symmetry are different.  Don't know how to handle this case!" << std::endl;
            exit(1);
        }

        // Number of layers.
        pandora::TiXmlElement* layers = (pandora::TiXmlElement*) calElem->FirstChild("layers");
        layers->QueryIntAttribute("nlayers", &nlayers);
        subdet.m_nLayers = nlayers;
        std::cout << subdet.m_nLayers.Get()  << " LAYERSSSSSS \n";

        // Process layer elements.
        pandora::TiXmlElement* layerElem = layers->FirstChildElement();
        //for (;
        //     layerElem;
        //while(     layerElem = layerElem->NextSiblingElement() )
           do
           {
            PandoraApi::Geometry::LayerParameters layerParams;
            DetectorGeometry::ExtraLayerParameters layerExtra;

            float dToIp = 0;
            float radLen = 0.;
            float intLen = 0.;
            float samplingFrac = 0.;
            float emSamplingFrac = 0.;
            float hadSamplingFrac = 0.;
            float cellThickness = 0.;

            // Get the layer parameters from the xml attributes.
            layerElem->QueryFloatAttribute("intLen", &intLen);
            layerElem->QueryFloatAttribute("radLen", &radLen);
            layerElem->QueryFloatAttribute("distanceToIp", &dToIp);
            layerElem->QueryFloatAttribute("samplingFraction", &samplingFrac);
            layerElem->QueryFloatAttribute("emSamplingFraction", &emSamplingFrac);
            layerElem->QueryFloatAttribute("hadSamplingFraction", &hadSamplingFrac);
            layerElem->QueryFloatAttribute("cellThickness", &cellThickness);
            
            // Set layer parameters.
            layerParams.m_closestDistanceToIp = dToIp;
            layerParams.m_nRadiationLengths = radLen;
            layerParams.m_nInteractionLengths = intLen;

            // Set extra layer parameters.
            layerExtra.m_samplingFraction = samplingFrac;
            layerExtra.m_emSamplingFraction = emSamplingFrac;
            layerExtra.m_hadSamplingFraction = hadSamplingFrac;
            layerExtra.m_cellThickness = cellThickness;
            layerExtra.m_intLength = intLen;
            layerExtra.m_radLength = radLen;

            // Register extra layer parameters.
            extras.m_extraLayerParams.push_back(layerExtra);

            // Add the layer to the subdetector's layer list.
            subdet.m_layerParametersVector.push_back(layerParams);
        } while(     layerElem = layerElem->NextSiblingElement() );

        // Set cell size information on extras.
        float cellSizeU = 0;
        float cellSizeV = 0;        
        calElem->QueryFloatAttribute("cellSizeU", &cellSizeU);
        calElem->QueryFloatAttribute("cellSizeV", &cellSizeV);
        extras.m_cellSizeU = cellSizeU;
        extras.m_cellSizeV = cellSizeV;
        extras.m_mipSigma = mipSigma;
        extras.m_mipCut = mipCut;
        extras.m_timeCut = timeCut;

        // Set the collection name on extras.
        extras.m_collection = calElem->Attribute("collection");

        // Setup the IDDecoder.
        IDDecoder::IDFields fields;
        pandora::TiXmlElement* idElem = (pandora::TiXmlElement*) calElem->FirstChild("id");
        pandora::TiXmlElement* fieldElem = (pandora::TiXmlElement*) idElem->FirstChild("field");
        //for (;
        //     fieldElem;
        do {
            std::string name;
            int length = 0;
            int start = 0;
            bool isSigned = false;
            std::string isSignedStr;

            name = fieldElem->Attribute("name");
            fieldElem->QueryIntAttribute("length", &length);
            fieldElem->QueryIntAttribute("start", &start);
            isSignedStr = fieldElem->Attribute("signed");
            if (isSignedStr == "true")
            {
                isSigned = true;
            }
            IDDecoder::IDField* field = new IDDecoder::IDField(name, start, length, isSigned);
            
            fields.push_back(field);            
        }   while(  fieldElem = fieldElem->NextSiblingElement());

        // Make an IDDecoder for this calorimeter.
        IDDecoder* decoder = new IDDecoder(fields);
        extras.m_decoder = decoder;

        // Set extra subdetector parameters.
        extras.m_inputHitType = getHitType(subdetType);
        extras.m_inputHitRegion = getHitRegion(subdetType);

        // Barrel inner Z is always 0.
        if (subdetTypeStr == "EM_BARREL" || subdetTypeStr == "HAD_BARREL" || subdetTypeStr == "MUON_BARREL")
        {
            subdet.m_innerZCoordinate = 0;
        }

        extras.m_mipEnergy = mipEnergy;
       
        // Calculate the subdetector normal vectors for a barrel region.
        if (extras.m_inputHitRegion.Get() == pandora::BARREL)
        {
            int nSides = innerSym;     
            double pi(std::acos(-1.));
            double dphi = -2 * pi / nSides;
            double phi0 = pi / 2.;
            double phi = phi0;
            for (int i = 0; i < nSides; ++i)
            {
                float x = cos(phi);
                float y = sin(phi);               
                if (std::fabs(x) < 1e-10) x=0;
                if (std::fabs(y) < 1e-10) y=0;
                extras.m_normalVectors.push_back(new pandora::CartesianVector(x, y, 0.));
                phi += dphi;
            }        
        }
	
        // Setting for digital or analog calorimeter.
        std::string isDigital = "false";
        calElem->QueryValueAttribute("digital", &isDigital);
        if (isDigital == "true")
        {
            extras.m_isDigital = true;
        }
        else
        {
            extras.m_isDigital = false;
        }        
        //if(extras.m_isDigital.IsInitialized()) {
        //   std::cout << " IsInitialized " << std::endl;
        //}

        // Insert the extra subdetector information into the data map.
        subdetExtras[subdetTypeStr] = extras;

        // Dump subdetector parameters to screen.
        std::cout << subdetTypeStr << std::endl;
#ifdef DETECTOR_GEOMETRY_DEBUG
        printOut(subdetType, &subdet);
#endif
        std::cout << subdet.m_nLayers.Get()  << " LAYERS VS ";
        std::cout << subdet.m_layerParametersVector.size()  << " LAYERS \n";

        PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::Geometry::SubDetector::Create(pandora, subdet));
    } while(  calElem = calElem->NextSiblingElement() );

    // Tracking parameters.
    pandora::TiXmlElement* tracking = root->FirstChildElement("tracking");
    float tinnerR, touterR, tz;
    tinnerR = touterR = tz = 0.f;
    tracking->QueryFloatAttribute("innerR", &tinnerR);
    tracking->QueryFloatAttribute("outerR", &touterR);
    tracking->QueryFloatAttribute("z", &tz);

    PandoraApi::Geometry::SubDetector::Parameters trackerParameters;
    trackerParameters.m_subDetectorName = "Tracker";
    trackerParameters.m_subDetectorType = pandora::INNER_TRACKER;
    trackerParameters.m_innerRCoordinate = tinnerR;
    trackerParameters.m_innerZCoordinate = 0.f;
    trackerParameters.m_innerPhiCoordinate = 0.f;
    trackerParameters.m_innerSymmetryOrder = 0;
    trackerParameters.m_outerRCoordinate = touterR;
    trackerParameters.m_outerZCoordinate = tz;
    trackerParameters.m_outerPhiCoordinate = 0.f;
    trackerParameters.m_outerSymmetryOrder = 0;
    trackerParameters.m_isMirroredInZ = true;
    trackerParameters.m_nLayers = 0;

#ifdef DETECTOR_GEOMETRY_DEBUG
    // Print tracking.
    std::cout << "Tracking:" << std::endl;
    std::cout << "    mainTrackerInnerRadius: " << trackerParameters.m_innerRCoordinate.Get() << std::endl;
    std::cout << "    mainTrackerOuterRadius: " << trackerParameters.m_outerRCoordinate.Get() << std::endl;
    std::cout << "    mainTrackerZExtent: " << trackerParameters.m_outerZCoordinate.Get() << std::endl;
    std::cout << std::endl;
#endif
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::Geometry::SubDetector::Create(pandora, trackerParameters));

    // Coil and B-field.
    pandora::TiXmlElement* coil = root->FirstChildElement("coil");
    float cinnerR, couterR, cz, bfield, coilRadLen = 0.f, coilIntLen = 0.f;
    cinnerR = couterR = cz = bfield = 0.;
    coil->QueryFloatAttribute("innerR", &cinnerR);
    coil->QueryFloatAttribute("outerR", &couterR);
    coil->QueryFloatAttribute("z", &cz);
    coil->QueryFloatAttribute("bfield", &bfield);
    coil->QueryFloatAttribute("radLen", &coilRadLen);
    coil->QueryFloatAttribute("intLen", &coilIntLen);

    m_innerBField = bfield;

    PandoraApi::Geometry::SubDetector::Parameters coilParameters;
    coilParameters.m_subDetectorName = "Coil";
    coilParameters.m_subDetectorType = pandora::COIL;
    coilParameters.m_innerRCoordinate = cinnerR;
    coilParameters.m_innerZCoordinate = 0.f;
    coilParameters.m_innerPhiCoordinate = 0.f;
    coilParameters.m_innerSymmetryOrder = 0;
    coilParameters.m_outerRCoordinate = couterR;
    coilParameters.m_outerZCoordinate = cz;
    coilParameters.m_outerPhiCoordinate = 0.f;
    coilParameters.m_outerSymmetryOrder = 0;
    coilParameters.m_isMirroredInZ = true;
    coilParameters.m_nLayers = 0;

#ifdef DETECTOR_GEOMETRY_DEBUG
    // Print coil and field.
    std::cout << "Coil:" << std::endl;
    std::cout << "    coilInnerRadius: " << coilParameters.m_innerRCoordinate.Get() << std::endl;
    std::cout << "    coilOuterRadius: " << coilParameters.m_outerRCoordinate.Get() << std::endl;
    std::cout << "    coilZExtent: " << coilParameters.m_outerZCoordinate.Get() << std::endl;
    std::cout << "    innerBField: " << this->getInnerBField() << std::endl;
    std::cout << std::endl;
#endif
    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::Geometry::SubDetector::Create(pandora, coilParameters));
}

pandora::SubDetectorType DetectorGeometry::getPandoraSubDetectorType(const std::string& subdetType) const
{
    return getPandoraSubDetectorType(subdetType.c_str());
}

pandora::SubDetectorType DetectorGeometry::getPandoraSubDetectorType(const char* subdetType) const
{    
    if ( strcmp( subdetType, "EM_BARREL" ) == 0 )
    {
        return pandora::ECAL_BARREL;
    }
    else if ( strcmp( subdetType, "EM_ENDCAP" ) == 0 )
    {
        return pandora::ECAL_ENDCAP;
    }
    else if ( strcmp( subdetType, "HAD_BARREL" ) == 0 )
    {
        return pandora::HCAL_BARREL;
    }
    else if ( strcmp( subdetType, "HAD_ENDCAP" ) == 0 )
    {
        return pandora::HCAL_ENDCAP;
    }
    else if ( strcmp( subdetType, "MUON_BARREL" ) == 0 )
    {
        return pandora::MUON_BARREL;
    }
    else if ( strcmp( subdetType, "MUON_ENDCAP" ) == 0 )
    {
        return pandora::MUON_ENDCAP;
    }
    else
    {
        return pandora::SUB_DETECTOR_OTHER;
    }
}

void DetectorGeometry::printOut(const char* subdetType, PandoraApi::Geometry::SubDetector::Parameters* subdet)
{
    // Parameters.
    std::cout << "Subdetector: " << subdetType << std::endl;
    std::cout << "    innerRCoordinate: " << subdet->m_innerRCoordinate.Get() << std::endl;
    std::cout << "    innerZCoordinate: " << subdet->m_innerZCoordinate.Get() << std::endl;
    std::cout << "    innerPhiCoordinate: " << subdet->m_innerPhiCoordinate.Get() << std::endl;
    std::cout << "    innerSymmetryOrder: " << subdet->m_innerSymmetryOrder.Get() << std::endl;
    std::cout << "    outerRCoordinate: " << subdet->m_outerRCoordinate.Get() << std::endl;
    std::cout << "    outerZCoordinate: " << subdet->m_outerZCoordinate.Get() << std::endl;
    std::cout << "    outerPhiCoordinate: " << subdet->m_outerPhiCoordinate.Get() << std::endl;
    std::cout << "    outerSymmetryOrder: " << subdet->m_outerSymmetryOrder.Get() << std::endl;
    std::cout << "    nLayers: " << subdet->m_nLayers.Get()  << std::endl;

    // Layers.
    int cntr = 1;
    for (auto iter = subdet->m_layerParametersVector.begin();
         iter != subdet->m_layerParametersVector.end();
         iter++ )
    {
        PandoraApi::Geometry::LayerParameters lp = (*iter);
        std::cout << "        layer " << cntr << " - dToIp=" << lp.m_closestDistanceToIp.Get() << ", radLen=" << lp.m_nRadiationLengths.Get() << ", intLen=" << lp.m_nInteractionLengths.Get() << std::endl;
        ++cntr;             
    }

    // Extras.
    std::string subdetTypeStr(subdetType);
    std::cout << "Extras : " << subdetTypeStr << std::endl;
    const DetectorGeometry::ExtraSubDetectorParameters& extras = subdetExtras[subdetTypeStr];
    std::cout << " LAYERS DERP\n";

    std::cout << "    digital=\n" << extras.m_isDigital.Get() << std::endl;

    IDDecoder* decoder = extras.m_decoder;
    std::cout << "    ID Fields (name, start, length, signed) - " << decoder->getFieldCount() << std::endl;           
    for (int i=0, j=decoder->getFieldCount(); i<j; i++)
    {
        const IDDecoder::IDField* field = decoder->getField(i);
        std::cout << "        " << field->getName() << ", " << field->getStart() << ", " << field->getLength() << ", " << field->isSigned() << std::endl;
    }
    
    printf("\n");    
}

pandora::InputHitType DetectorGeometry::getHitType(const std::string& calType) const
{
    if (calType == "EM_BARREL" || calType == "EM_ENDCAP")
    {
        return pandora::ECAL;
    }
    else if (calType == "HAD_BARREL" || calType == "HAD_ENDCAP")
    {
        return pandora::HCAL;
    } 
    else if (calType == "MUON_BARREL" || calType == "MUON_ENDCAP")
    {
        return pandora::MUON;
    }
    else
    {
        std::cout << "Unknown CalorimeterType <" << calType << ">." << std::endl;
    }                
    return pandora::ECAL;
}

inline pandora::InputHitRegion DetectorGeometry::getHitRegion(const std::string& calType) const
{
    if (calType == "EM_BARREL" || calType == "HAD_BARREL" || calType == "MUON_BARREL")
    {
        return pandora::BARREL;
    }
    else if (calType == "EM_ENDCAP" || calType == "HAD_ENDCAP" || calType == "MUON_ENDCAP")
    {
        return pandora::ENDCAP;
    }
    else
    {
        std::cout << "Unknown CalorimeterType <" << calType << ">." << std::endl;
    }
    return pandora::BARREL;
}

    
