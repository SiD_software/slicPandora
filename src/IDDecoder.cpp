#include "IDDecoder.h"

int IDDecoder::getFieldValue(int index, long id)
{
    IDField* field = m_indexMap[index];
    int start = field->getStart();
    int length = field->getLength();
    int mask = (1<<length) - 1;
    int result = (int) ((id >> start) & mask);
    if (field->isSigned())
    {
        int signBit = 1<<(length-1);
        if ((result & signBit) != 0) result -= (1<<length);
    }
    return result;
}

int IDDecoder::getFieldValue(const std::string& name, long id)
{
    return getFieldValue(getFieldIndex(name), id);
}



