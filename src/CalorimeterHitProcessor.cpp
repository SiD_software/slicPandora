// $Id: CalorimeterHitProcessor.cpp,v 1.24 2012/01/19 00:00:22 jeremy Exp $

#include "CalorimeterHitProcessor.h"

// lcio
#include "EVENT/LCCollection.h"

// pandora
#include "Api/PandoraApi.h"
#include "Managers/GeometryManager.h"
#include "Objects/SubDetector.h"

// slicPandora
#include "IDDecoder.h"
#include "JobManager.h"
#include "JobConfig.h"
#include "DetectorGeometry.h"

// stl
#include <cmath>

using std::fabs;
using EVENT::CalorimeterHit;
using EVENT::LCCollection;

void CalorimeterHitProcessor::processEvent(EVENT::LCEvent* event)
{
    // Get the parameter objects necessary for CaloHit conversion to Pandora.
    const pandora::Pandora& pandora = getJobManager()->getPandora();
    DetectorGeometry* geom = getJobManager()->getDetectorGeometry();
    const LcioInputCollectionSettings::CaloCollectionMap& caloCollMap = getJobManager()->getLcioCollectionSettings().getCaloCollectionMap();

    for (LcioInputCollectionSettings::CaloCollectionMap::const_iterator iter = caloCollMap.begin();
            iter != caloCollMap.end();
            iter++)
    {
        std::string caloType = LcioInputCollectionSettings::getStringFromType(iter->first);

        // Get the SubDetector parameters.
        const pandora::SubDetectorType subDetectorType(geom->getPandoraSubDetectorType(caloType));
        const pandora::SubDetector &subdet(pandora.GetGeometry()->GetSubDetector(subDetectorType));
        DetectorGeometry::ExtraSubDetectorParameters* xsubdet = geom->getExtraSubDetectorParametersFromType(caloType);

        const LCCollection* caloHits;

        // Check again if collection exists.  If not, could be okay so skip and move on.
        try 
        {
            caloHits = event->getCollection(caloType);
        }
        catch (...)
        {
            std::cout << "Could not get collection " << caloType << " from event." << std::endl;
            throw new std::exception;
        }

        int nhits = caloHits->getNumberOfElements();

        for (int i=0; i<nhits; i++)
        {
            CalorimeterHit* calHit = dynamic_cast<CalorimeterHit*> (caloHits->getElementAt(i));
            PandoraApi::CaloHit::Parameters caloHitParams = makeCaloHitParameters(&subdet, xsubdet, calHit);
                      
            PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::CaloHit::Create(pandora, caloHitParams));
        }
    }
}

PandoraApi::CaloHit::Parameters CalorimeterHitProcessor::makeCaloHitParameters(
    const pandora::SubDetector* subdet,
    DetectorGeometry::ExtraSubDetectorParameters* xsubdet,
    CalorimeterHit* hit)
{
    // Create a new, empty CaloHit Parameters.
    PandoraApi::CaloHit::Parameters params;

    // Get the IDDecoder.
    IDDecoder* decoder = xsubdet->m_decoder;

    // Make a 64-bit id for the hit.
    long long cellId = makeId64(hit);

    // Get the layer number.
    int layer = decoder->getFieldValue("layer", cellId);

    // Get the layer parameters for this layer.
    const pandora::SubDetector::SubDetectorLayerVector &layerList = subdet->GetSubDetectorLayerVector();
    const int nlayers = layerList.size();

    // Set isInOuterSamplingLayer.
    if (xsubdet->m_inputHitType.Get() == pandora::HCAL)
    {
        if (layer >= nlayers - 3)
        {
            params.m_isInOuterSamplingLayer = true;
        }
        else
        {
            params.m_isInOuterSamplingLayer = false;
        }
    }
    else if (xsubdet->m_inputHitType.Get() == pandora::MUON)
    {
        params.m_isInOuterSamplingLayer = true;
    }
    else if (xsubdet->m_inputHitType.Get() == pandora::ECAL)
    {
        params.m_isInOuterSamplingLayer = false;
    }

    // Get layer parameters.
    const pandora::SubDetector::SubDetectorLayer &layerParams = layerList[layer];
    DetectorGeometry::ExtraLayerParameters xlayerParams = xsubdet->m_extraLayerParams[layer];

    // Mip Energy.
    float mipEnergy = xsubdet->m_mipEnergy.Get();

    // Sampling fractions.
    float samplingFraction = xlayerParams.m_samplingFraction.Get();
    float emSamplingFraction = xlayerParams.m_emSamplingFraction.Get();
    float hadSamplingFraction = xlayerParams.m_hadSamplingFraction.Get();

    // Recover the raw energy with reverse calculation.
    // Should work for both digital and analog because sampling fraction 
    // is applied in SimCalorimeterHitProcessor for both places.
    float rawEnergy = hit->getEnergy() * samplingFraction;        

    // Get the module number.
    int module = decoder->getFieldValue("module", cellId);

    // Get the hit position.
    const float* pos(hit->getPosition());   

    // Position in mm.
    params.m_positionVector = pandora::CartesianVector(pos[0], pos[1], pos[2]);
    params.m_expectedDirection = pandora::CartesianVector(pos[0], pos[1], pos[2]).GetUnitVector();

    // Copy energy from CalHit. 
    params.m_inputEnergy = hit->getEnergy();

    // Get the digital calorimeter setting from the associated subdetector parameters.
    params.m_isDigital = xsubdet->m_isDigital; 

    // For digital hits, divide 1 by the sampling fractions.
    if (params.m_isDigital.Get() == true)
    {
        // EM energy in GeV.
        params.m_electromagneticEnergy = 1. / emSamplingFraction;
        
        // HAD energy in GeV.
        params.m_hadronicEnergy = 1. / hadSamplingFraction;
        
        // MIP equivalent energy.
        params.m_mipEquivalentEnergy = 1. / mipEnergy; 
    }
    // For analog hits, divide the raw energy by the sampling fractions.
    else
    {
        // EM energy in GeV.
        params.m_electromagneticEnergy = rawEnergy / emSamplingFraction;
        
        // HAD energy in GeV.
        params.m_hadronicEnergy = rawEnergy / hadSamplingFraction;
        
        // MIP equivalent energy.
        params.m_mipEquivalentEnergy = rawEnergy / mipEnergy; 
    }

    // Layer number.
    params.m_layer = layer;

    // Hit time in ns.
    params.m_time = hit->getTime();

    // Pointer to the source hit.
    params.m_pParentAddress = hit; 
    
    // Cell size U in mm.
    params.m_cellSize0 = xsubdet->m_cellSizeU;

    // Cell size V in mm.
    params.m_cellSize1 = xsubdet->m_cellSizeV;

    // Cell geometry, rectangular or pointing
    params.m_cellGeometry = pandora::RECTANGULAR;
    
    // Cell thickness in mm.
    params.m_cellThickness = xlayerParams.m_cellThickness; 

    // Number of radiation lengths in layer.
    params.m_nCellRadiationLengths = layerParams.GetNRadiationLengths();

    // Number of interaction lengths in layer.
    params.m_nCellInteractionLengths = layerParams.GetNInteractionLengths();

    // Type of hit.
    params.m_hitType = xsubdet->m_inputHitType;

    // Type of hit region.
    params.m_hitRegion = xsubdet->m_inputHitRegion;

    // Barrel case uses module.
    if (xsubdet->m_inputHitRegion.Get() == pandora::BARREL)
    {
        params.m_cellNormalVector = *(xsubdet->m_normalVectors.at(module));
    }
    // Endcap case is computed.
    else
    {
        params.m_cellNormalVector = pandora::CartesianVector(0., 0., pos[2]/fabs(pos[2]));
    }

    // Return completed calorimeter parameters.
    return params;
}


void CalorimeterHitProcessor::printCaloHitParameters(const PandoraApi::CaloHit::Parameters& params)
{
    std::cout << params.m_positionVector.Get() << " [mm]" << std::endl;
    std::cout << "    energy: " << params.m_inputEnergy.Get() << " [GeV]" << std::endl;
    std::cout << "    layer: " << params.m_layer.Get() << std::endl;
    std::cout << "    time: " << params.m_time.Get() << " [ns]" << std::endl;
    std::cout << "    cell sizes: " << params.m_cellSize0.Get() << ", " << params.m_cellSize1.Get() << " [mm]" << std::endl;
    std::cout << "    cell thick: " << params.m_cellThickness.Get() << " [mm]" << std::endl;
    std::cout << "    radLengths: " << params.m_nCellRadiationLengths.Get() << std::endl;
    std::cout << "    intLengths: " << params.m_nCellInteractionLengths.Get() << std::endl;
    std::cout << "    normalVec: " << params.m_cellNormalVector.Get() << std::endl;
    std::cout << "    hitType: " << params.m_hitType.Get() << std::endl;
    std::cout << "    hitRegion: " << params.m_hitRegion.Get() << std::endl;
    std::cout << "    outerLayer: " << params.m_isInOuterSamplingLayer.Get() << std::endl;
}
