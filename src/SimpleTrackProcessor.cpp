// $Id: SimpleTrackProcessor.cpp,v 1.25 2011/09/19 18:54:31 jeremy Exp $

//#define POST_CDR_VERSION 1


#include "SimpleTrackProcessor.h"

// slicPandora
#include "JobManager.h"

// lcio
#include "EVENT/LCCollection.h"
#include "EVENT/Track.h"
#include "EVENT/LCGenericObject.h"
#include "EVENT/LCObject.h"
#include "EVENT/MCParticle.h"
#include "UTIL/LCRelationNavigator.h"

// pandora
#include "Api/PandoraApi.h"
#include "Managers/GeometryManager.h"
#include "Managers/PluginManager.h"
#include "Objects/CartesianVector.h"
#include "Objects/Helix.h"
#include "Objects/MCParticle.h"
#include "Objects/SubDetector.h"
#include "Plugins/BFieldPlugin.h"


// stl
#include <string>
#include <cmath>
#include <limits>

using EVENT::LCCollection;
using EVENT::Track;
using EVENT::LCObjectVec;
using EVENT::LCGenericObject;
using UTIL::LCRelationNavigator;
using pandora::CartesianVector;

// FIXME: Name of collection containing LCIO Track objects is hard-coded to "Tracks".
//std::string SimpleTrackProcessor::trackCollectionName = "Tracks";
//std::string SimpleTrackProcessor::startCollectionName = trackCollectionName + "_StateAtStart";
//std::string SimpleTrackProcessor::ecalCollectionName = trackCollectionName + "_StateAtECal";
//std::string SimpleTrackProcessor::endCollectionName = trackCollectionName + "_StateAtEnd";

void SimpleTrackProcessor::processEvent(EVENT::LCEvent* event)
{
    // Get the name of the Track collection from the job config.
    const std::string& trackCollectionName = getJobManager()->getLcioCollectionSettings().getTrackCollectionName();

    LCCollection* trackCollection = NULL;
    try
    {
        trackCollection = event->getCollection(trackCollectionName);
    }
    catch(...)
    {
        std::cout << "no track collection with the name '" << trackCollectionName << "' found." << std::endl;
        return;
    }

    // Check if MC information is present.
    bool haveMCRelations = false;
    const EVENT::LCCollection *pMCRelationCollection = 0;
    UTIL::LCRelationNavigator* navigate = NULL;
    
    // Look for LCRelations collection of HelicalTrackHits to MCParticles.
    try 
    {
        pMCRelationCollection = event->getCollection("HelicalTrackMCRelations");
    }
    catch (EVENT::DataNotAvailableException &exception)
    {}    

    // Found relations collection.
    if (pMCRelationCollection != 0)
    {
        haveMCRelations = true;
        navigate = new UTIL::LCRelationNavigator(pMCRelationCollection);
    }


    // Get the B field.
    const pandora::Pandora& pandora = getJobManager()->getPandora();
    const float magneticField(pandora.GetPlugins()->GetBFieldPlugin()->GetBField(CartesianVector(0.f,0.f,0.f)));


    // Loop over input tracks.
    int ntracks = trackCollection->getNumberOfElements();
    for (int i=0; i<ntracks; i++)
    {
#ifdef SIMPLETRACKPROCESSOR_DEBUG
        std::cout << "proc track #" << i << std::endl;
#endif

        // Get the current Track.
        Track* track = dynamic_cast<Track*>(trackCollection->getElementAt(i));

        // Setup a new Track parameters object.
        PandoraApi::Track::Parameters trackParameters;

        // Get the pointer to the LCIO Track.
        trackParameters.m_pParentAddress = track;

        // Set impact parameters.
        trackParameters.m_d0 = track->getD0();
        trackParameters.m_z0 = track->getZ0();
	
        // Set the sign.
        const float signedCurvature(track->getOmega());
        if (0. != signedCurvature)
            trackParameters.m_charge = static_cast<int>(signedCurvature / std::fabs(signedCurvature));

        // FIXME: Mass hard-coded to charged pion.
        trackParameters.m_mass = 0.13957018;

        // FIXME: Particle Id hard-coded to charged pion.
        if (signedCurvature > 0.)
            trackParameters.m_particleId = 211;
        else
            trackParameters.m_particleId = -211;

	// per default all tracks are in the barrel
        trackParameters.m_isProjectedToEndCap = false; // FIXME


        // Setup track states from GenericObject collections, assuming that the ordering matches that in the "Tracks" collection.
        setupTrackStatesFromGenericObjects(trackParameters, event, i);

        // Set the momentum at DCA to track's start state.  
        trackParameters.m_momentumAtDca = trackParameters.m_trackStateAtStart.Get().GetMomentum();

        // Print debug info if enabled.
#ifdef SIMPLETRACKPROCESSOR_DEBUG
        std::cout << "Track Parameters: " << std::endl;
        std::cout << "    d0 = " << trackParameters.m_d0.Get() << std::endl;
        std::cout << "    z0 = " << trackParameters.m_z0.Get() << std::endl;
        std::cout << "    mass = " << trackParameters.m_mass.Get() << std::endl;
        std::cout << "    momentumAtDca = " << trackParameters.m_momentumAtDca.Get() << std::endl;
        std::cout << "    stateAtStart  = " << trackParameters.m_trackStateAtStart.Get() << std::endl;
        std::cout << "    stateAtEnd    = " << trackParameters.m_trackStateAtEnd.Get() << std::endl;
        std::cout << "    stateAtCalorimeter = " << trackParameters.m_trackStateAtCalorimeter.Get() << std::endl;        
	std::cout << "    reachesCalorimeter = " << trackParameters.m_reachesCalorimeter.Get() << std::endl;
        std::cout << "    canFormPfo              = " << trackParameters.m_canFormPfo.Get() << std::endl;
        std::cout << "    canFormClusterlessPfo   = " << trackParameters.m_canFormClusterlessPfo.Get() << std::endl;
        std::cout << "    isProjectedToEndCap     = " << trackParameters.m_isProjectedToEndCap.Get() << std::endl;
	std::cout << "    Number of Hits on Track = " << track->getTrackerHits().size() << std::endl;
        std::cout << "    parentAddress = " << trackParameters.m_pParentAddress.Get() << std::endl;
	std::cout << "    " << std::endl;
#endif      

#ifdef USE_PANDORA_TRACK_EXTRAP
//experimental code for the timing adapted from the MArlinPandora Code

        pandora::Helix *MyHelix = new pandora::Helix(track->getPhi(), track->getD0(), track->getZ0(), track->getOmega(), track->getTanLambda(), magneticField); 
 	const pandora::CartesianVector &referencePoint(MyHelix->GetReferencePoint());

 	
	float minGenericTime(std::numeric_limits<float>::max());
	float genericTime(std::numeric_limits<float>::max());
	
	pandora::CartesianVector bestECalProjection;
	pandora::CartesianVector barrelProjection; 
	// First project to endcap
	(void) MyHelix->GetPointInZ(trackParameters.m_charge.Get()* (pandora::GeometryHelper::GetECalEndCapParameters().GetInnerZCoordinate()), referencePoint, bestECalProjection, minGenericTime);	 
        // Cylinder, dealing with cylinders only right now
        
        const pandora::StatusCode statusCode(MyHelix->GetPointOnCircle(pandora::GeometryHelper::GetECalBarrelParameters().GetInnerRCoordinate(), referencePoint, barrelProjection, genericTime));

        if ((pandora::STATUS_CODE_SUCCESS == statusCode) && (genericTime < minGenericTime))
        {
        	minGenericTime = genericTime;
	        
	        bestECalProjection = barrelProjection;
        
        }
   

        // Convert generic time (length from reference point to intersection, divided by momentum) into nanoseconds
        const float particleMass(trackParameters.m_mass.Get());
        const float particleEnergy(std::sqrt(particleMass * particleMass + trackParameters.m_momentumAtDca.Get().GetMagnitudeSquared()));
        trackParameters.m_timeAtCalorimeter = minGenericTime * particleEnergy / 300.f; 
#endif





        // Register Track parameters with Pandora.
        PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::Track::Create(pandora, trackParameters));

        // Setup the Track to MCParticle relations (optional).
        EVENT::TrackerHitVec trackHits = track->getTrackerHits();
        if (haveMCRelations)
        {
            try
            {
                EVENT::MCParticle *pBestMCParticle = NULL;
                for (EVENT::TrackerHitVec::const_iterator itTh = trackHits.begin(), itThEnd = trackHits.end(); itTh != itThEnd; ++itTh )
                {
                    EVENT::TrackerHit* trackerHit = (*itTh);
                    const EVENT::LCObjectVec &objectVec = navigate->getRelatedToObjects(trackerHit);
                    
                    // Get reconstructed momentum at dca
                    const pandora::Helix helixFit(track->getPhi(), track->getD0(), track->getZ0(), track->getOmega(), track->getTanLambda(), magneticField);
                    const float recoMomentum(helixFit.GetMomentum().GetMagnitude());
                    
                    // Use momentum magnitude to identify best mc particle
                    float bestDeltaMomentum(std::numeric_limits<float>::max());
                    
                    for (EVENT::LCObjectVec::const_iterator itRel = objectVec.begin(), itRelEnd = objectVec.end(); itRel != itRelEnd; ++itRel)
                    {
                        EVENT::MCParticle *pMCParticle = NULL;
                        pMCParticle = dynamic_cast<EVENT::MCParticle *>(*itRel);
                        
                        if (NULL == pMCParticle)
                            continue;
                        
                        const float trueMomentum(pandora::CartesianVector(pMCParticle->getMomentum()[0], pMCParticle->getMomentum()[1],
                                                                          pMCParticle->getMomentum()[2]).GetMagnitude());
                        
                        const float deltaMomentum(std::fabs(recoMomentum - trueMomentum));
                        
                        if (deltaMomentum < bestDeltaMomentum)
                        {
                            pBestMCParticle = pMCParticle;
                            bestDeltaMomentum = deltaMomentum;
                        }
                    }
                }
                
                if (NULL == pBestMCParticle)
                    continue;
                
                PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::SetTrackToMCParticleRelationship(pandora, track,
                                                                                                              pBestMCParticle));
            }                    
            catch (pandora::StatusCodeException &statusCodeException)
            {
                std::cout << "Failed to extract track to mc particle relationship: " << statusCodeException.ToString() << std::endl;
            }
            catch (EVENT::Exception &exception)
            {
                std::cout << "Failed to extract track to mc particle relationship: " << exception.what() << std::endl;
            }
        }
    }

    // Delete LCRelationsNavigator if exists.
    if (navigate != 0)
        delete navigate;
}

void SimpleTrackProcessor::setupTrackStatesFromGenericObjects(PandoraApi::Track::Parameters& trackParameters, EVENT::LCEvent* event, int i)
{       
    // Get the Track state collections.
    LcioInputCollectionSettings settings = this->getJobManager()->getLcioCollectionSettings();
    LCCollection* startCollection = event->getCollection(settings.getTrackStateCollectionName("StateAtStart"));
    LCCollection* ecalCollection = event->getCollection(settings.getTrackStateCollectionName("StateAtECal"));
    LCCollection* endCollection = event->getCollection(settings.getTrackStateCollectionName("StateAtEnd"));

    // Add start state.
    LCGenericObject* startObj = dynamic_cast<LCGenericObject*>(startCollection->getElementAt(i));
    trackParameters.m_trackStateAtStart = 
        pandora::TrackState(startObj->getFloatVal(0), 
                            startObj->getFloatVal(1), 
                            startObj->getFloatVal(2), 
                            startObj->getFloatVal(3), 
                            startObj->getFloatVal(4), 
                            startObj->getFloatVal(5));
    
    // Add ECal state.
    LCGenericObject* ecalObj = dynamic_cast<LCGenericObject*>(ecalCollection->getElementAt(i));
    // FIXME: quick fix to set the trackStateAtECal to the trackStateAtEnd if the ecalObj is filled with nan float values. 
    // this is the case for pt < 1.0GeV
    if(std::isnan(ecalObj->getFloatVal(0)))
    {
        	
	LCGenericObject* exceptional_endObj = dynamic_cast<LCGenericObject*>(endCollection->getElementAt(i));
        trackParameters.m_trackStateAtCalorimeter = pandora::TrackState(exceptional_endObj->getFloatVal(0), 
                                                                exceptional_endObj->getFloatVal(1), 
                                                                exceptional_endObj->getFloatVal(2), 
                                                                exceptional_endObj->getFloatVal(3), 
                                                                exceptional_endObj->getFloatVal(4), 
                                                                exceptional_endObj->getFloatVal(5));
	trackParameters.m_reachesCalorimeter = false;	//right to assume it does not reach the endcap							
	trackParameters.m_canFormClusterlessPfo  = true; // it can still form a clusterless PFO
	trackParameters.m_canFormPfo = false;	
    }
    else
    {
        trackParameters.m_trackStateAtCalorimeter = 
            pandora::TrackState(ecalObj->getFloatVal(0), 
                                ecalObj->getFloatVal(1), 
                                ecalObj->getFloatVal(2), 
                                ecalObj->getFloatVal(3), 
                                ecalObj->getFloatVal(4), 
                                ecalObj->getFloatVal(5));
	trackParameters.m_reachesCalorimeter = true; // does reach ECAL
	trackParameters.m_canFormClusterlessPfo  = false;
	trackParameters.m_canFormPfo = true;	// use a full PFO
    }
    // this will set the timing cut to infinity, till we have something better ...
    trackParameters.m_timeAtCalorimeter = std::numeric_limits<float>::max(); // FIXME Temporarily set to large value to fail any timing cuts
 
    // Add end state.
    LCGenericObject* endObj = dynamic_cast<LCGenericObject*>(endCollection->getElementAt(i));
    trackParameters.m_trackStateAtEnd = pandora::TrackState(endObj->getFloatVal(0), 
                                                            endObj->getFloatVal(1), 
                                                            endObj->getFloatVal(2), 
                                                            endObj->getFloatVal(3), 
                                                            endObj->getFloatVal(4), 
                                                            endObj->getFloatVal(5));
    // figure out, whether this one goes to the endcap using the StateAtECal fields
    const pandora::Pandora& pandora = getJobManager()->getPandora();

    const float innerZ=pandora.GetGeometry()->GetSubDetector(pandora::ECAL_ENDCAP).GetInnerZCoordinate();
    float trackZ=fabs(trackParameters.m_trackStateAtCalorimeter.Get().GetPosition().GetZ());

    // FIXME Hard-coded check for projection to EndCap.
    if (trackZ-innerZ > -1.5) // it is close to the endcap,
    {
        trackParameters.m_isProjectedToEndCap=true; //it is in the endcap
    }
    else
    {
        trackParameters.m_isProjectedToEndCap=false;   //stays in barrel
    }
}
