#include "PfoConstructionAlgorithm.h"

#include "Api/PandoraApi.h"
#include "Api/PandoraContentApi.h"
#include "Xml/tinyxml.h"

using namespace pandora;

pandora::StatusCode PfoConstructionAlgorithm::Run()
{
    // Algorithm code here
    const ClusterList *pClusterList = NULL;
    PANDORA_RETURN_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraContentApi::GetCurrentList(*this, pClusterList));

    for (ClusterList::const_iterator iter = pClusterList->begin(), iterEnd = pClusterList->end(); iter != iterEnd; ++iter)
    {
        const Cluster *pCluster = *iter;

        PandoraContentApi::ParticleFlowObject::Parameters particleFlowObjectParameters;
        
        // FIXME: Dummy parameters.
        particleFlowObjectParameters.m_particleId = 1;                          
        particleFlowObjectParameters.m_charge = -1;                         
        particleFlowObjectParameters.m_mass = 1.;                               
        particleFlowObjectParameters.m_energy = 1.;                             
        particleFlowObjectParameters.m_momentum = CartesianVector(1, 2, 3);     

        // Make a new pfo for every cluster.
        particleFlowObjectParameters.m_clusterList.insert(iter,pCluster);            

        const ParticleFlowObject *pParticleFlowObject(NULL);
        PANDORA_RETURN_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraContentApi::ParticleFlowObject::Create(*this, particleFlowObjectParameters, pParticleFlowObject));
    }

    return pandora::STATUS_CODE_SUCCESS;
}

//------------------------------------------------------------------------------------------------------------------------------------------

pandora::StatusCode PfoConstructionAlgorithm::ReadSettings(const TiXmlHandle xmlHandle)
{
    // Read settings from xml file here

    return pandora::STATUS_CODE_SUCCESS;
}
