#include "DummyProcessor.h"

// stl
#include <iostream>

DummyProcessor::DummyProcessor()
    : EventProcessor("DummyProcessor")
{}

DummyProcessor::~DummyProcessor()
{}

void DummyProcessor::processEvent(EVENT::LCEvent*)
{
    std::cout << "DummyProcessor got event!" << std::endl;
}
