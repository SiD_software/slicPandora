// $Id: PandoraFrontend.cpp,v 1.3 2013/07/03 19:47:03 jeremy Exp $
#include "PandoraFrontend.h"

// slicPandora
#include "JobManager.h"
#include "JobConfig.h"
#include "DefaultProcessors.h"

#include <iostream>

#include <getopt.h>

using std::cout;
using std::endl;

void PandoraFrontend::printUsage()
{
    cout << endl;
    cout << "-----slicPandora Usage-----" << endl;
    cout << "./bin/PandoraFrontend -g [geometry] -c [pandoraConfig] -i [inputEvents] -o [outputEvents] -l [lcioConfig] -r [nrun] -s [nskip] -f" << endl << endl;
    cout << "    [-r] is optional.  Default is run over all input events." << endl;
    cout << "    [-s] is optional.  Default is start at the first event." << endl;
    cout << "    [-l] is optional.  Default is use subdetector's associated hit collections from geometry." << endl;
    cout << "    [-o] is optional.  Default output file is called \"pandoraOutput.slcio\"." << endl;
    cout << "    [-f] is optional.  This flag will cause existing input collections that slicPandora creates to be replaced." << endl;
}

int PandoraFrontend::run(int argc, char **argv)
{
    // getopt flags.
    int g;
    opterr = 0;

    // Variables for holding getopt values.
    int nrun = -1;
    int nskip = 0;
    char *geometryFile = NULL;
    char *configFile = NULL;
    char *outputFile = "pandoraOutput.slcio";
    char *lcioConfigFile = NULL;
    char *inputFile = NULL;
    bool deleteExistingCollections = false;

    // Process command line options.
    while ((g = getopt(argc, argv, "g:c:i:r:s:l:o:f")) != -1)
    {
        switch (g)
        {
            // Geometry file.
            case 'g':
                geometryFile = optarg;
                break;
            // Pandora config file.
            case 'c':
                configFile = optarg;
                break;
            // Output file path.
            case 'o':
                outputFile = optarg;
                break;
            // Input LCIO file.
            case 'i':
                inputFile = optarg;
                break;
            // LCIO XML collection config file.
            case 'l':
                lcioConfigFile = optarg;
                break;
            // Number of events to run.
            case 'r':
                nrun = atoi(optarg);
                break;
            // Number of events to skip.
            case 's':
                nskip = atoi(optarg);
                break;
            // Force input collections to be recreated.
            case 'f':
            	deleteExistingCollections = true;
            	break;
            // There are no valid non-switch arguments.
            default:
                printUsage();
                return 1;
        }
    }

    // Print arguments before we do anything with them.
    cout << "slicPandora got these command line arguments..." << endl;
    cout << endl;
    if (geometryFile != NULL)
    {
        cout << "geometryFile (-g) = " << geometryFile << endl;
    }
    if (configFile != NULL)
    {
        cout << "configFile (-c) = " << configFile << endl;
    }
    if (outputFile != NULL)
    {
        cout << "outputFile (-o) = " << outputFile << endl;
    }
    if (inputFile != NULL)
    {
        cout << "inputFile (-i) = " << inputFile << endl;
    }
    if (lcioConfigFile != NULL)
    {
        cout << "lcioCollFile (-l) = " << lcioConfigFile << endl;
    }
    if (nrun > 0)
    {
        cout << "nrun (-r) = " << nrun << endl;
    }
    if (nskip > 0)
    {
        cout << "nskip (-s) = " << nskip << endl;
    }
    if (deleteExistingCollections)
    {
    	cout << "deleteExistingCollections (-f) = " << deleteExistingCollections << endl;
    }
    cout << endl;

    // Error flag.
    bool error = false;

    // Create the job configuration from the command line arguments.
    JobConfig* config = new JobConfig();

    // Pandora config.
    if (configFile != NULL)
    {
        config->setPandoraSettingsXmlFile(configFile);
    }
    else
    {
        cout << "Missing config file." << endl;
        error = true;
    }

    // Geometry file.
    if (geometryFile != NULL)
    {
        config->setGeometryFile(geometryFile);
    }
    else
    {
        cout << "Missing geometry file." << endl;
        error = true;
    }

    // Input file.
    if (inputFile != NULL)
    {
        config->addInputFile(inputFile);
    }
    else
    {
        cout << "Missing input file." << endl;
        error = true;
    }

    // Output file.  Don't need to check for null because it has a default.
    config->setOutputFile(outputFile);

    // Skip events.
    if (nskip != 0)
    {
        config->setSkipEvents(nskip);
    }

    // LCIO config file.
    if (lcioConfigFile != NULL)
    {
        config->setLcioConfigFile(lcioConfigFile);
    }

    // Number of events to run.
    config->setNumberOfEvents(nrun);

    // Set delete existing collections.
    config->setDeleteExistingCollections(deleteExistingCollections);

    // Stop the job if command line arguments were not good.
    if (error)
    {
        printUsage();
        return 1;
    }

    // Make a new job manager.
    JobManager* mgr = new JobManager();

    // Set the JobManager's configuration.
    mgr->setJobConfig(config);

    // Add a processor to mark beginning of event processing.
    mgr->addEventProcessor(new EventMarkerProcessor());

    // Add a processor to convert LCIO MCParticles to Pandora MCParticle::Parameters.
    mgr->addEventProcessor(new MCParticleProcessor());

    // Add a processor to convert LCIO SimCalorimeterHits to LCIO CalorimeterHits.
    mgr->addEventProcessor(new SimCalorimeterHitProcessor());

    // Add a processor to convert LCIO CalorimeterHits to Pandora CaloHit::Parameters.
    mgr->addEventProcessor(new CalorimeterHitProcessor());

    // Add a processor to convert LCIO Tracks and their track states to Pandora Track Parameters.
    mgr->addEventProcessor(new SimpleTrackProcessor());

    // Add a processor to automatically run the registered Pandora algorithms.
    mgr->addEventProcessor(new PandoraProcessor());

    // Add a processor to create LCIO PFO objects, including clusters and ReconParticles.
    mgr->addEventProcessor(new PfoProcessor());

    // Add a processor to reset Pandora after the event.
    mgr->addEventProcessor(new ResetPandoraProcessor());

    // Run the job.
    mgr->run();

    // Return success.
    return 0;
}
