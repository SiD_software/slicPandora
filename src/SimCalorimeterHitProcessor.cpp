#include "SimCalorimeterHitProcessor.h"

// pandora
#include "Api/PandoraApi.h"

// slicPandora
#include "JobManager.h"
#include "JobConfig.h"
#include "DetectorGeometry.h"
#include "IDDecoder.h"
#include "LcioInputCollectionSettings.h"

// lcio
#include "EVENT/LCIO.h"
#include "EVENT/LCCollection.h"
#include "IMPL/LCCollectionVec.h"
#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCRelationImpl.h"
#include "IMPL/LCFlagImpl.h" 
#include "EVENT/SimCalorimeterHit.h"

// std
#include <stdexcept>

using EVENT::LCCollection;
using IMPL::LCFlagImpl;
using IMPL::CalorimeterHitImpl;
using IMPL::LCCollectionVec;
using IMPL::LCRelationImpl;
using EVENT::SimCalorimeterHit;

using EVENT::MCParticle;

SimCalorimeterHitProcessor::SimCalorimeterHitProcessor()
    : EventProcessor("SimCalorimeterHitProcessor")
{;}

SimCalorimeterHitProcessor::~SimCalorimeterHitProcessor()
{;}

void SimCalorimeterHitProcessor::processEvent(EVENT::LCEvent* event)
{
    // Get pointers to parameter and manager classes.
    JobManager* mgr = getJobManager();
    const pandora::Pandora& pandora = mgr->getPandora();
    DetectorGeometry* geom = mgr->getDetectorGeometry();
    const LcioInputCollectionSettings& lcioConfig = mgr->getLcioCollectionSettings();
    const LcioInputCollectionSettings::CaloCollectionMap& caloCollMap = lcioConfig.getCaloCollectionMap();

    // Check that the detector names match between file and geometry.
    if (event->getDetectorName().compare(geom->getDetectorName()) != 0)
    {
        throw std::runtime_error("Detector name from LCIO file doesn't match geometry.");
    }

    // Make the relation table from SimCalorimeterHit to corresponding CalorimeterHit.
    LCCollectionVec* scRel = new LCCollectionVec(EVENT::LCIO::LCRELATION);
    scRel->parameters().setValue("RelationFromType", EVENT::LCIO::CALORIMETERHIT);
    scRel->parameters().setValue("RelationToType", EVENT::LCIO::SIMCALORIMETERHIT);
    
    typedef std::map<MCParticle *, float> MCParticleToEnergyWeightMap;
    MCParticleToEnergyWeightMap mcParticleToEnergyWeightMap;

    // Set correct flags for output collection.
    int flag = 1 << EVENT::LCIO::RCHBIT_LONG; // position
    flag |= 1 << EVENT::LCIO::RCHBIT_ID1;     // cellId1
    flag |= 1 << EVENT::LCIO::RCHBIT_TIME;    // time
    LCFlagImpl chFlag(flag);

    // Delete existing collections.
    bool deleteExistingCollections = mgr->getJobConfig()->deleteExistingCollections();

    // Loop over CaloTypes.
    for (LcioInputCollectionSettings::CaloCollectionMap::const_iterator it = caloCollMap.begin(); it != caloCollMap.end(); it++)
    {
        // Get the CaloType string.
        const std::string& caloType = LcioInputCollectionSettings::getStringFromType(it->first);

        // Delete an existing CalHit collection.
        if (deleteExistingCollections)
        {
        	event->removeCollection(caloType);
        }

        // Skip over unknown types of cal collections.
        if (caloType.compare("UNKNOWN") == 0)
        {
            std::cout << "Skipping unknown type of calorimeter collection!" << std::endl;
            continue;
        }

        // Get the extra subdet params from this cal type.
        DetectorGeometry::ExtraSubDetectorParameters* xsubdet = geom->getExtraSubDetectorParametersFromType(caloType);
        if (xsubdet == NULL)
        {
            std::cout << "The ExtraSubDetectorParameters for " << caloType << " were not found." << std::endl;
            throw new std::exception;
        }

        // Get the decoder.
        IDDecoder* decoder = xsubdet->m_decoder;

        // Get cuts from xsubdet.
        float mipCut = xsubdet->m_mipCut.Get();
        float timeCut = xsubdet->m_timeCut.Get();

        // Create a new LCIO CalHit collection.
        LCCollection* calHits = new LCCollectionVec(EVENT::LCIO::CALORIMETERHIT);
        calHits->setFlag(chFlag.getFlag());

        // Loop over input collection names.  All input collections will be merged into collection by type,
        // so all the subdetectors of one CaloType should be from the same subdetector.
        const std::vector<std::string>& collections = it->second;
        for (std::vector<std::string> ::const_iterator it2 = collections.begin(); it2 != collections.end(); it2++)
        {
            // Get the collection name.
            const std::string& collectionName = (*it2);

            // Lookup CalHit collection in input event.
            LCCollection* simCalHits = 0;
            try
            {
                simCalHits = event->getCollection(collectionName);
            }
            catch (EVENT::DataNotAvailableException& de)
            {
                std::cout << "Failed to get SimCalHit collection " << collectionName << " from event." << std::endl;
                throw new std::exception;
            }
            catch (std::exception& e)
            {
                std::cout << "Caught unknown std exception trying to access " << collectionName << " from event." << std::endl;
                throw new std::exception;
            }

            // Get the number of SimCalorimeterHits.
            int nSimHits = simCalHits->getNumberOfElements();

            // Loop over input SimCalorimeterHits and convert them to CalorimeterHits.
            for (int i = 0; i < nSimHits; i++)
            {
                // Get the SimCalorimeterHit to be converted from the LCIO collection.
                SimCalorimeterHit* simCalHit = dynamic_cast<SimCalorimeterHit*> (simCalHits->getElementAt(i));

                // Get the first time contrib.
                float timeCont = simCalHit->getTimeCont(0);

                // Get the raw energy deposition.
                float rawEnergy = simCalHit->getEnergy();

                // Apply the time cut from the subdetector params.
                if (timeCont > timeCut)
                {
                    continue;
                }

                // Apply MIP energy cut from the subdetector params.
                if (rawEnergy < mipCut)
                {
                    continue;
                }

                // Create a new CalorimeterHit.
                CalorimeterHitImpl* calHit = new CalorimeterHitImpl;

                // Get the two 32-bit chunks of the ID.
                int cellId0 = simCalHit->getCellID0();
                int cellId1 = simCalHit->getCellID1();

                // Make a 64-bit id for the IDDecoder.  The type MUST be "long long" and not "long".  (from Tony Johnson)
                long long cellId = ((long long)cellId1) << 32 | cellId0;

                // Decode the layer number from the ID.
                int layer = decoder->getFieldValue("layer", cellId);

                // Get the extra parameters for this layer.
                float samplingFrac = 0.f;
                try
                {
                    // Get the layer parameters.
                    DetectorGeometry::ExtraLayerParameters xlayerParams = xsubdet->m_extraLayerParams.at(layer);

                    // Get the sampling fraction for this layer.
                    samplingFrac = xlayerParams.m_samplingFraction.Get();

                    // TODO: Separate EM + HAD sampling fractions.
                }
                // FIXME What exception is caught here?
                catch(...)
                {
                    std::cout << "No layer #" << layer << " existing in detector " << caloType << std::endl;
                    throw;
                }

                // Set hit energy.
                if (xsubdet->m_isDigital.Get()) 
                {
                    // Digital energy is one over the sampling fraction.
                    calHit->setEnergy(1. / samplingFrac);
                }
                else
                {
                    // Analog energy is the raw energy over the sampling fraction.
                    calHit->setEnergy(simCalHit->getEnergy() / samplingFrac);
                }

                // Copy SimCalorimeterHit information into CalorimeterHit
                calHit->setCellID0(simCalHit->getCellID0());
                calHit->setCellID1(simCalHit->getCellID1());
                calHit->setTime(simCalHit->getTimeCont(0));
                calHit->setPosition(simCalHit->getPosition());

                // Setup the relation between CalHit and SimHit.
                scRel->addElement(new LCRelationImpl(calHit, simCalHit, 0.5));

                // FIXME Need to add links for CalorimeterHit collections that already exist.
                // FIXME Is this code to set energy weighting really needed or used in recon?
                mcParticleToEnergyWeightMap.clear();
                for (int iCont = 0, iEnd = simCalHit->getNMCContributions(); iCont < iEnd; ++iCont)
                {
                    mcParticleToEnergyWeightMap[simCalHit->getParticleCont(iCont)] += simCalHit->getEnergyCont(iCont);
                }

                for (MCParticleToEnergyWeightMap::const_iterator mcParticleIter = mcParticleToEnergyWeightMap.begin(),
                        mcParticleIterEnd = mcParticleToEnergyWeightMap.end(); mcParticleIter != mcParticleIterEnd; ++mcParticleIter)
                {
                    PANDORA_THROW_RESULT_IF(pandora::STATUS_CODE_SUCCESS, !=, PandoraApi::SetCaloHitToMCParticleRelationship(pandora,     calHit     , mcParticleIter->first, mcParticleIter->second));
                }

                // Add the created CalorimeterHit to the collection.
                calHits->addElement(calHit);
            }

            // Set the ReadoutName parameter for LCSim to find the correct IDDecoder.
            // FIXME This should actually be in the outer loop but collectionName is no longer available.
            calHits->parameters().setValue("ReadoutName", collectionName);
        }

        // Add the CalorimeterHits to the event with the collection name the same as the calorimeter type, e.g. EM_BARREL.
        event->addCollection(calHits, caloType);
    }

    // Add the CalorimeterHit to SimCalorimeterHit relations.
    if (deleteExistingCollections)
    {
    	event->removeCollection("CalorimeterHitRelations");
    }
    event->addCollection(scRel, "CalorimeterHitRelations");
}
