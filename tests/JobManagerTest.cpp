// $Id: JobManagerTest.cpp,v 1.7 2010/03/17 02:12:03 jeremy Exp $

// slicPandora
#include "JobConfig.h"
#include "JobManager.h"
#include "DefaultProcessors.h"

/**
 * This main function runs a complete Pandora job on example input using the JobManager.
 * It MUST be run from the slicPandora root directory using this command. 
 *
 *     ./bin/JobManagerTest
 *
 * If this program is called from another directory, it will not be able to find the 
 * input data files and will crash.
 *
 * The user must provide an input LCIO file called input.slcio containing a slic event.
 * This can be a sym link or an actual LCIO file.
 */
int main(int argc, char** argv)
{
    // Create an example job configuration.
    JobConfig* config = new JobConfig();
    config->setPandoraSettingsXmlFile("./examples/PandoraSettingsNew.xml");
    config->setGeometryFile("./examples/sidloi2_pandora.xml");
    config->useDefaultCalorimeterTypes();
    //config->addCalorimeterType("EM_BARREL"); // DEBUG: Just use EM_BARREL collection.
    config->addInputFile("./input.slcio");
    config->setOutputFile("pandoraRecon.slcio");
    config->setNumberOfEvents(2);
    config->setSkipEvents(0);

    // Make a new job manager.
    JobManager* mgr = new JobManager();

    // Set the JobManager's configuration object.
    mgr->setJobConfig(config);

    // Add a processor to mark beginning of event processing.
    mgr->addEventProcessor(new EventMarkerProcessor());

    // Add a processor to convert LCIO SimCalorimeterHits to LCIO CalorimeterHits.
    mgr->addEventProcessor(new SimCalorimeterHitProcessor());

    // Add a processor to convert LCIO CalorimeterHits to Pandora CaloHit::Parameters.
    mgr->addEventProcessor(new CalorimeterHitProcessor());

    // Add a processor to automatically run the registered Pandora algorithms.
    mgr->addEventProcessor(new PandoraProcessor());

    // Add a processor to create LCIO PFO objects, including clusters and ReconstructedParticles.
    mgr->addEventProcessor(new PfoProcessor());

    // Add a processor to reset Pandora after the event.
    mgr->addEventProcessor(new ResetPandoraProcessor());

    // Run the job.
    mgr->run();
}
