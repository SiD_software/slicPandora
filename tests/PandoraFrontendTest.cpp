// $Id: PandoraFrontendTest.cpp,v 1.1 2011/09/19 19:50:10 jeremy Exp $

/**
 * Test of PandoraFrontend, which is used as the default binary for slicPandora.
 */
#include "PandoraFrontend.h"

// Run main.
int main(int argc, char **argv)
{
    PandoraFrontend pandora;
    return pandora.run(argc, argv);
}
