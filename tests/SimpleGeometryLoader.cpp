#include "DetectorGeometry.h"

using namespace std;

int main( int argc, const char* argv[] )
{
    if ( argc < 2 ) 
    {
        std::cout << "USAGE: ./slicPandora geometry.xml" << std::endl;
        return 1;
    }

    const char* geomFile = argv[1];

    DetectorGeometry geom;
    geom.loadFromFile( geomFile );

    PandoraApi::Geometry::Parameters* params = geom.getGeometryParameters();

    // test 
    std::cout << "trackerInnerRadius = " << params->m_mainTrackerInnerRadius.Get() << std::endl;
    std::cout << "trackerOuterRadius = " << params->m_mainTrackerOuterRadius.Get() << std::endl;
}
