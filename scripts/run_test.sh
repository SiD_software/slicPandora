#!/bin/sh
testcase=${1}
${testcase} ${@} &> /dev/null
ret=$?
res=Success
if [ "$ret" -ne 0 ]; then
   res=Failure
fi
# Write to test log.
echo "TEST: ${testcase}, RESULT: $res, RETURN_CODE: $ret" >> test.log
